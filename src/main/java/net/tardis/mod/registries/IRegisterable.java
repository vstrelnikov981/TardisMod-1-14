package net.tardis.mod.registries;

import net.minecraft.util.ResourceLocation;
/**
 * Implement this interface for any object that needs to be registered via TardisRegistries
 * <br> Deprecated as of 1.16 Forge, moved all custom registries to custom Deferred Registers
 * @author Spectre0987
 *
 * @param <T>
 */
@Deprecated
public interface IRegisterable<T> {

	T setRegistryName(ResourceLocation regName);
	ResourceLocation getRegistryName();
}
