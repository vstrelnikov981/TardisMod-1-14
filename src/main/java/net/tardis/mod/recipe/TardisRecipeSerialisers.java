package net.tardis.mod.recipe;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TardisRecipeSerialisers {
	
	public static final DeferredRegister<IRecipeSerializer<?>> RECIPE_SERIALISERS = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, Tardis.MODID);
	
	public static final RegistryObject<IRecipeSerializer<?>> QUANTISCOPE_SERIALISER = RECIPE_SERIALISERS.register("quantiscope", () -> new WeldRecipe.WeldRecipeSerializer());
	public static final ResourceLocation WELD_RECIPE_TYPE_LOC = new ResourceLocation(Tardis.MODID, "quantiscope");
	public static final IRecipeType<WeldRecipe> WELD_RECIPE_TYPE = IRecipeType.register(WELD_RECIPE_TYPE_LOC.toString());
	
    public static final RegistryObject<IRecipeSerializer<?>> ALEMBIC_SERIALISER = RECIPE_SERIALISERS.register("alembic", () -> new AlembicRecipe.AlembicRecipeSerializer());
	public static final ResourceLocation ALEMBIC_TYPE_LOC = new ResourceLocation(Tardis.MODID, "alembic");
	public static final IRecipeType<AlembicRecipe> ALEMBIC_RECIPE_TYPE = IRecipeType.register(ALEMBIC_TYPE_LOC.toString());
	
	public static final RegistryObject<IRecipeSerializer<?>> ATTUNEABLE_SERIALISER = RECIPE_SERIALISERS.register("attunable", () -> new AttunableRecipe.AttunableRecipeSerializer());
	public static final ResourceLocation ATTUNEABLE_TYPE_LOC = new ResourceLocation(Tardis.MODID, "attunable");
	public static final IRecipeType<AttunableRecipe> ATTUNEABLE_RECIPE_TYPE = IRecipeType.register(ATTUNEABLE_TYPE_LOC.toString());

}
