package net.tardis.mod.misc;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.items.misc.AttunableItem;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AttunementProgressMessage;
import net.tardis.mod.recipe.AttunableRecipe;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.tileentities.inventory.PanelInventoryWrapper;

public class AttunementHandler {

    public static final int ATTUNEMENT_SLOT = 4;

    private Supplier<PanelInventory> inventorySupplier;
    private World world;
    private int attunmentTime;
    private int maxAttunmentTime;
    private AttunableRecipe recipe;

    //Only for client, to update progress bar
    private float attunmentProgress = 0;

    public AttunementHandler(Supplier<PanelInventory> inv, World world){
        this.inventorySupplier = inv;
        this.world = world;
    }

    //If ticking, in flight
    public void tick(ConsoleTile tile){
        if(this.isAttuning()){
            this.maxAttunmentTime = this.getMax();
            ++this.attunmentTime;
            if(this.attunmentTime >= this.maxAttunmentTime)
                this.complete(tile);
            else this.updateClients();
        }
        else if(needsResetProgress())
            this.setAttuning(ItemStack.EMPTY);
    }

    public boolean needsResetProgress() {
        return this.attunmentTime != 0 || this.maxAttunmentTime != 0;
    }

    public boolean isAttuning(){

        //If attunable
        if(this.getAttuningItem().getItem() instanceof IAttunable){
            return true;
        }
        //Look for recipe, if none found, return false, else true
        return (this.recipe = this.findRecipe()) != null;

    }

    public void complete(ConsoleTile tile){

        ItemStack attuningItem = this.getAttuningItem();

        if(attuningItem.getItem() instanceof IAttunable){
            this.inventorySupplier.get().setStackInSlot(ATTUNEMENT_SLOT, ((IAttunable)attuningItem.getItem()).onAttuned(attuningItem, tile));
        }
        //else recipe stuff

        if(this.recipe != null){
        	ItemStack resultStack = recipe.getRecipeOutput();
        	if (attuningItem.getCount() > recipe.getRecipeOutput().getCount()) //Account for itemstack whose count is larger than 1
    			resultStack.setCount(attuningItem.getCount());
            this.inventorySupplier.get().setStackInSlot(ATTUNEMENT_SLOT, recipe.shouldAddNBTTags() ? AttunableItem.completeAttunement(resultStack, tile) : resultStack);
        }

        this.setAttuning(ItemStack.EMPTY);

    }

    public void serialize(CompoundNBT nbt){
        nbt.putInt("attunement_ticks", this.attunmentTime);
    }

    public void deserialize(CompoundNBT nbt){
        this.attunmentTime = nbt.getInt("attunement_ticks");
    }

    public ItemStack getAttuningItem(){
        return inventorySupplier.get().getStackInSlot(ATTUNEMENT_SLOT);
    }

    public void setAttuning(ItemStack itemStack){
        //Reset if empty
        if(itemStack.isEmpty()){
            this.attunmentTime = 0;
            this.maxAttunmentTime = 0;
            return;
        }
        //If attunable
        if(itemStack.getItem() instanceof IAttunable){
            this.attunmentTime = 0;
            this.attunmentProgress = 0;
            this.maxAttunmentTime = ((IAttunable)itemStack.getItem()).getAttunementTime();
        }
        //If recipe
        this.recipe = this.findRecipe();
        if(this.recipe != null){
            this.attunmentTime = 0;
            this.attunmentProgress = 0;
            this.maxAttunmentTime = recipe.getAttunementTicks();
        }
        this.updateClients();
    }

    public AttunableRecipe findRecipe(){
        for (AttunableRecipe recipe : AttunableRecipe.getAllRecipes(world)) {
            if (recipe.matches(new PanelInventoryWrapper(inventorySupplier.get()), world)) {
                this.recipe = recipe;
            }
        }
        return null;
    }

    public int getMax(){
        ItemStack stack = this.getAttuningItem();

        if(stack.getItem() instanceof IAttunable)
            return ((IAttunable)stack.getItem()).getAttunementTime();

        if((this.recipe = this.findRecipe()) != null) {
        	if (stack.getCount() > recipe.getRecipeOutput().getCount()) {
        		int adjustedTime = this.recipe.getAttunementTicks() * stack.getCount();
        		return adjustedTime;
        	}
        	else {
        		return this.recipe.getAttunementTicks();
        	}
        }

        return 0;

    }

    public void updateClients(){
        Network.sendToAllInWorld(new AttunementProgressMessage(this.attunmentTime / (float)this.maxAttunmentTime), (ServerWorld)world);
    }

    public float getAttunementProgress() {
        return this.attunmentProgress;
    }

    public void setProgress(float progress) {
        this.attunmentProgress = progress;
    }
}
