package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
/** Base Class for Vortex Manipualator Functions
 * <br>
 * Extend BaseVortexMFunction to implement your custom functions.
 * During FMLCommonSetup, add your Function to a VortexMFunctionCategory to allow it to show up on the GUI
 * <br> If none of the existing categories are suitable, register you own
 * */
public abstract class AbstractVortexMFunction extends ForgeRegistryEntry<AbstractVortexMFunction>{
	/**
     * Execute Client side code here E.g. Opening a GUI
     * <br> Do NOT directly call clientside methods like Minecraft#getInstance#displayScreen, this can crash a dedicated server
     * <br> Instead, call clientside methods from a static method in another class
     * <br> E.g. ClientHelper#openGUI()
     *
     * @param world
     * @param player
     * @implNote Usually for Client side only
     */
    public abstract void sendActionOnClient(World world, PlayerEntity player);

    /**
     * Execute serverside code here
     * <br> E.g. Scanning the area for entities
     *
     * @param world
     * @implNote Do not call this from the gui, as you cannot cast a ClientWorld to ServerWorld
     */
    public abstract void sendActionOnServer(World world, ServerPlayerEntity player);

    public abstract String getTranslationKey();
    
    /**
     * Name of the function that shows in the GUI textbox
     */
    public abstract TranslationTextComponent getDisplayName();

    /**
     * Returns if the player can access this function
     */
    public boolean stateComplete() {return true;}

    /**
     * Determine which logical sides this function can be called from
     * <br>VortexMUseType.CLIENT for CLIENT side only (world#isRemote)
     * <br>VortexMUseType.SERVER for SERVER side only (!world#isRemote)
     * <br>VortexMUseType.BOTH to allow the function to be able to execute code for both client and server
     * <br> If BOTH or SERVER is used, a VMFunctionMessage will be sent to the server
     */
    public abstract VortexMUseType getLogicalSide();

}
