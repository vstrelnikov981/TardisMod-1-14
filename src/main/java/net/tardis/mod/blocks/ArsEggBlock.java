package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
//import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.StringTextComponent;
//import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.GuiContext;
//import net.minecraftforge.api.distmarker.Dist;
//import net.minecraftforge.fml.DistExecutor;
//import net.tardis.mod.Tardis;
//import net.tardis.mod.client.guis.minigame.circuit.CircuitGame;
//import net.tardis.mod.constants.Constants;
//import net.tardis.mod.helper.Helper;
//import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.properties.Prop;

public class ArsEggBlock extends Block {

    public static VoxelShape SHAPE = createVoxelShape();

    public ArsEggBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    public static VoxelShape createVoxelShape() {
        VoxelShape shape = VoxelShapes.create(0.359375, 0.625, 0.359375, 0.640625, 0.6875, 0.640625);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.390625, 0.6875, 0.390625, 0.609375, 0.703125, 0.609375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.953125, 0.4375, 0.5625, 0.96875, 0.5625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.421875, 0.671875, 0.421875, 0.578125, 0.875, 0.578125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.453125, 0.84375, 0.453125, 0.546875, 1.0, 0.546875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.890625, 0.4375, 0.5625, 0.90625, 0.5625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.921875, 0.4375, 0.5625, 0.9375, 0.5625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.5, 0.375, 0.625, 0.625, 0.625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.3125, 0.0625, 0.3125, 0.6875, 0.5, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.015625, 0.375, 0.625, 0.0625, 0.625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.34375, 0.03125, 0.34375, 0.65625, 0.59375, 0.65625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.328125, 0.046875, 0.328125, 0.671875, 0.546875, 0.671875), IBooleanFunction.OR);
        return shape;
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return super.getStateForPlacement(context).with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());

    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

    	
        if (!WorldHelper.isDimensionBlocked(worldIn)) {
            if (worldIn.isRemote)
                ClientHelper.openGUI(Constants.Gui.ARS_EGG, new GuiContext());
        } else if (!worldIn.isRemote()) {
            player.sendStatusMessage(Constants.Translations.NO_USE_OUTSIDE_TARDIS, true);
        }
        
        
        
        return ActionResultType.SUCCESS;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

}
