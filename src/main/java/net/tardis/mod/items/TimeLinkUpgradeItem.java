package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants.Part.PartType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TimeLinkUpgradeItem extends TardisPartItem{

	public TimeLinkUpgradeItem() {
		super(PartType.UPGRADE, false, false);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		TileEntity te = context.getWorld().getTileEntity(context.getPos());
		if(te instanceof ConsoleTile) {
			setConsoleDimension(context.getItem(), te.getWorld().getDimensionKey());
			return ActionResultType.SUCCESS;
		}
		return super.onItemUse(context);
	}
	
	public static void setConsoleDimension(ItemStack stack, RegistryKey<World> dim) {
		stack.getOrCreateTag().putString("console", dim.getLocation().toString());
	}
	
	public static RegistryKey<World> getConsolePos(ItemStack stack) {
		if(!stack.hasTag() || !stack.getTag().contains("console"))
			return null;
		return RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(stack.getTag().getString("console")));
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		RegistryKey<World> dim = getConsolePos(stack);
		if(dim != null)
			tooltip.add(new TranslationTextComponent("tooltip.tardis.timelink", dim.getLocation().getPath()));
	}


}