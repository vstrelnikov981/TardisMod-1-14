package net.tardis.mod.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

import java.text.DecimalFormat;
import java.util.List;


/**
 * A debug tool for developers to use when adding new features
 */
public class DebugItem extends Item {


    /**
     * I amware that items are singletons, thanks
     * <p>
     * However, this item wont even be registered outside
     * a dev enviroment, so stfu
     */
    public double offsetY = 0;
    public float width = 0.0625F, height = 0.0625F;
    public AxisAlignedBB box = new AxisAlignedBB(0, 0, 0, 0, 0, 0);


    public DebugItem() {
        super(new Item.Properties().maxStackSize(64));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (!context.getWorld().isRemote()) {
            if (context.getWorld().getTileEntity(context.getPos()) instanceof ConsoleTile) {
                ConsoleTile te = (ConsoleTile) context.getWorld().getTileEntity(context.getPos());
                if (te != null) {
                    te.removeControls();
                    te.getOrCreateControls();
                }
            }
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        tooltip.add(new StringTextComponent("Refreshes Console Unit controls on Right click, Highlights existing controls and helps position new Controls"));
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity player) {
        if (!player.world.isRemote)
            return false;

        DecimalFormat form = new DecimalFormat("###.###");
        System.out.println("return EntitySize.flexible(" + box.getXSize() + "F, " + box.getYSize() + "F)" + ";");

        Vector3d pos = new Vector3d(
                box.getCenter().x - 0.5,
                box.getCenter().y - TardisHelper.TARDIS_POS.getY() + 1.09375 - 0.0625,
                box.getCenter().z - 0.5
        );

        System.out.println("return new Vector3d(" + form.format(pos.x) + ", " + form.format(pos.getY()) + ", " + form.format(pos.getZ()) + ");");

        return false;
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entity, int itemSlot, boolean isSelected) {

        if (!(entity instanceof PlayerEntity) || !worldIn.isRemote)
            return;
        PlayerEntity player = (PlayerEntity) entity;

        double rot = Math.toRadians(player.rotationYawHead - 180);
        double x = player.getPosX() + Math.sin(rot), z = player.getPosZ() - Math.cos(rot);
        double y = TardisHelper.TARDIS_POS.getY() + offsetY;

        box = new AxisAlignedBB(x - width / 2.0, y, z - width / 2.0, x + width / 2.0, y + height, z + width / 2.0);
    }
}
