package net.tardis.mod.flight;

import java.util.ArrayList;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.tileentities.ConsoleTile;

public class VerticalFlightEvent extends FlightEvent{

	public VerticalFlightEvent(FlightEventFactory entry, ArrayList<ResourceLocation> control) {
		super(entry, control);
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		tile.getControl(LandingTypeControl.class).ifPresent(control -> {
			control.setLandType(control.getLandType() == EnumLandType.DOWN ? EnumLandType.UP : EnumLandType.DOWN);
			tile.updateClient();
		});
		
	}

	
}
