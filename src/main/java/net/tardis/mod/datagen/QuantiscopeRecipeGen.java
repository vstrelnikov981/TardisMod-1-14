package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
/**
 * Generate recipes and repair recipes for Quantiscope
 */
public class QuantiscopeRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public QuantiscopeRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        createNormalRecipe(path, cache, TItems.KEY_FOB_UPGRADE.get(), Ingredient.fromItems(TItems.CIRCUITS.get(), Items.ENDER_PEARL, Items.GOLD_INGOT));
    }

    @Override
    public String getName() {
        return "TARDIS Quantiscope Recipe Generator";
    }
    
    public static Path getPath(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + ".json");
    }
    
    public static Path getPathRepair(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + "_repair" + ".json");
    }

    /**
     * Create a non-repair recipe. Make sure it is a unique recipe
     * @param output
     * @param ingredients - items that are used to craft the output item. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, Ingredient ingredients) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(output, ingredients), getPath(base, output));
    }
    
    /**
     * Create a repair recipe. Make sure it is a unique recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - items that are used to repair itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createRepairRecipe(Path base, DirectoryCache cache, Item itemToRepair, Ingredient repairItems) throws IOException {
        IDataProvider.save(GSON, cache, this.createRepairRecipe(itemToRepair, repairItems), getPathRepair(base, itemToRepair));
    }
    
    /**
     * Creates a non-repair recipe. Make sure it is a unique recipe
     * @param output - this is the item that is output at the end
     * @param ingredients - the items that are used to craft output. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRecipe(Item output, Ingredient ingredients) {
        JsonObject root = new JsonObject();

        root.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
        root.addProperty("repair", false);
        root.add("ingredients", ingredients.serialize());
        JsonObject result = new JsonObject();
        result.addProperty("item", output.getRegistryName().toString());
        root.add("result", result);
        
        return root;
    }
    
    /**
     * Creates a repair recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - the items that are used to repair the itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRepairRecipe(Item itemToRepair, Ingredient repairItems) {
        JsonObject root = new JsonObject();
        
        //We must add the "type" field so vanilla can identify our recipes as a custom recipe
        root.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
        root.addProperty("repair", true);
        root.addProperty("repair_item", itemToRepair.getRegistryName().toString());
        root.add("ingredients", repairItems.serialize());
        
        return root;
    }

}
