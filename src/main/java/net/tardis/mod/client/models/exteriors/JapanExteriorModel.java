package net.tardis.mod.client.models.exteriors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
//import net.tardis.mod.helper.ModelHelper;
//import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class JapanExteriorModel extends ExteriorModel {
	private final ModelRenderer Roof;
	private final ModelRenderer Posts;
	private final ModelRenderer Signage;
	private final ModelRenderer panels;
	private final ModelRenderer lAmp;
	private final ModelRenderer door;
	private final LightModelRenderer glow_lamp;
	private final ModelRenderer boti;

	public JapanExteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		Roof = new ModelRenderer(this);
		Roof.setRotationPoint(14.0F, -18.0F, -14.0F);
		Roof.setTextureOffset(112, 0).addBox(-27.0F, -4.0F, 1.0F, 26.0F, 3.0F, 26.0F, 0.0F, false);
		Roof.setTextureOffset(0, 32).addBox(-25.0F, -6.0F, 3.0F, 22.0F, 2.0F, 22.0F, 0.0F, false);

		Posts = new ModelRenderer(this);
		Posts.setRotationPoint(-12.0F, 20.0F, 10.0F);
		Posts.setTextureOffset(131, 31).addBox(-1.0F, -39.0F, -23.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);
		Posts.setTextureOffset(161, 70).addBox(-3.0F, -40.0F, -25.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(161, 70).addBox(23.0F, -40.0F, -25.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(161, 70).addBox(-3.0F, -40.0F, 1.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-2.0F, -39.0F, 2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(24.0F, -39.0F, 2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(24.0F, -39.0F, -24.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-2.0F, -39.0F, -24.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(25.0F, -42.0F, 3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-3.0F, -42.0F, 3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(25.0F, -42.0F, -25.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-3.0F, -42.0F, -25.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(161, 70).addBox(23.0F, -40.0F, 1.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(131, 31).addBox(22.0F, -39.0F, -23.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);
		Posts.setTextureOffset(131, 31).addBox(22.0F, -39.0F, 0.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);
		Posts.setTextureOffset(131, 31).addBox(-1.0F, -39.0F, 0.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);
		Posts.setTextureOffset(0, 0).addBox(-2.0F, 0.0F, -24.0F, 28.0F, 4.0F, 28.0F, 0.0F, false);

		Signage = new ModelRenderer(this);
		Signage.setRotationPoint(14.0F, -18.0F, -14.0F);
		Signage.setTextureOffset(32, 78).addBox(-26.0F, -1.0F, 0.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);
		Signage.setTextureOffset(42, 87).addBox(-28.0F, -1.0F, 2.0F, 3.0F, 4.0F, 24.0F, 0.0F, false);
		Signage.setTextureOffset(32, 78).addBox(-26.0F, -1.0F, 25.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);
		Signage.setTextureOffset(42, 87).addBox(-3.0F, -1.0F, 2.0F, 3.0F, 4.0F, 24.0F, 0.0F, false);

		panels = new ModelRenderer(this);
		panels.setRotationPoint(9.0F, 20.0F, -12.0F);
		panels.setTextureOffset(88, 32).addBox(2.0F, -35.0F, 2.0F, 1.0F, 35.0F, 20.0F, 0.0F, false);
		panels.setTextureOffset(88, 32).addBox(-21.0F, -35.0F, 2.0F, 1.0F, 35.0F, 20.0F, 0.0F, false);
		panels.setTextureOffset(0, 86).addBox(-19.0F, -35.0F, 0.0F, 20.0F, 35.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(96, 87).addBox(-9.0F, -35.0F, 23.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

		lAmp = new ModelRenderer(this);
		lAmp.setRotationPoint(14.0F, -24.0F, -14.0F);
		lAmp.setTextureOffset(147, 33).addBox(-17.0F, -2.0F, 11.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
		lAmp.setTextureOffset(147, 41).addBox(-17.0F, -7.0F, 11.0F, 6.0F, 1.0F, 6.0F, 0.0F, false);
		lAmp.setTextureOffset(145, 70).addBox(-16.0F, -9.0F, 12.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		lAmp.setTextureOffset(153, 76).addBox(-15.0F, -11.0F, 13.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		lAmp.setTextureOffset(161, 76).addBox(-12.5F, -6.0F, 11.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		lAmp.setTextureOffset(161, 76).addBox(-16.5F, -6.0F, 11.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		lAmp.setTextureOffset(161, 76).addBox(-16.5F, -6.0F, 15.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		lAmp.setTextureOffset(161, 76).addBox(-12.5F, -6.0F, 15.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		door = new ModelRenderer(this);
		door.setRotationPoint(-6.0F, 20.0F, 10.0F);
		door.setTextureOffset(118, 87).addBox(-4.0F, -35.0F, 0.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

		glow_lamp = new LightModelRenderer(this);
		glow_lamp.setRotationPoint(1.0F, -26.0F, 0.0F);
		glow_lamp.setTextureOffset(147, 48).addBox(-3.0F, -4.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		boti = new ModelRenderer(this);
		boti.setRotationPoint(9.0F, 20.0F, 9.0F);
		boti.setTextureOffset(0, 126).addBox(-19.0F, -35.0F, 0.0F, 20.0F, 35.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		matrixStack.push();
		matrixStack.translate(EnumDoorType.JAPAN.getRotationForState(tile.getOpen()), 0, 0);
		door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		matrixStack.pop();
		
		Roof.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		Posts.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		Signage.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		panels.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		lAmp.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		glow_lamp.setBright(tile.getLightLevel());
		glow_lamp.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		boti.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	}
}