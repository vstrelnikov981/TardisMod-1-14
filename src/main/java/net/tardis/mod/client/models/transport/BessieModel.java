package net.tardis.mod.client.models.transport;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.client.models.misc.EntityGlowRenderer;
import net.tardis.mod.client.renderers.RenderText;
import net.tardis.mod.entity.BessieEntity;

public class BessieModel extends EntityModel<BessieEntity> implements RenderText.IRenderText {
    private final List<RenderText> textToRender = new ArrayList<>();
    private final ModelRenderer LIGHTME;
    
    private final ModelRenderer Floor;
    private final ModelRenderer bone13;
    private final ModelRenderer bone;
    private final ModelRenderer bone2;
    private final ModelRenderer FrontLicensePlate;
    private final ModelRenderer RightWall;
    private final ModelRenderer UnderSeatToCarrage2;
    private final ModelRenderer right_walls;
    private final ModelRenderer back_right;
    private final ModelRenderer front_right;
    private final ModelRenderer Engine2;
    private final ModelRenderer bone10;
    private final ModelRenderer bone21;
    private final ModelRenderer Fenders2;
    private final ModelRenderer Front2;
    private final ModelRenderer bone22;
    private final ModelRenderer bone23;
    private final ModelRenderer bone24;
    private final ModelRenderer bone25;
    private final ModelRenderer bone26;
    private final ModelRenderer Back2;
    private final ModelRenderer bone62;
    private final ModelRenderer bone63;
    private final ModelRenderer bone64;
    private final ModelRenderer bone65;
    private final ModelRenderer Edging2;
    private final ModelRenderer bone66;
    private final ModelRenderer bone67;
    private final ModelRenderer bone68;
    private final ModelRenderer bone69;
    private final ModelRenderer bone70;
    private final ModelRenderer bone71;
    private final ModelRenderer bone72;
    private final ModelRenderer LeftWall;
    private final ModelRenderer UnderSeatToCarrage;
    private final ModelRenderer front_left2;
    private final ModelRenderer front_left;
    private final ModelRenderer Engine;
    private final ModelRenderer bone3;
    private final ModelRenderer bone4;
    private final ModelRenderer Fenders;
    private final ModelRenderer Front;
    private final ModelRenderer bone5;
    private final ModelRenderer bone8;
    private final ModelRenderer bone9;
    private final ModelRenderer bone6;
    private final ModelRenderer bone7;
    private final ModelRenderer Back;
    private final ModelRenderer bone11;
    private final ModelRenderer bone12;
    private final ModelRenderer bone14;
    private final ModelRenderer bone15;
    private final ModelRenderer Edging;
    private final ModelRenderer bone16;
    private final ModelRenderer bone17;
    private final ModelRenderer bone19;
    private final ModelRenderer bone18;
    private final ModelRenderer bone20;
    private final ModelRenderer bone60;
    private final ModelRenderer bone61;
    private final ModelRenderer CrossBeams;
    private final ModelRenderer WindowFrame;
    private final ModelRenderer Supports;
    private final ModelRenderer Axles;
    private final ModelRenderer front;
    private final ModelRenderer Wheel_FrontRight;
    private final ModelRenderer south2;
    private final ModelRenderer east2;
    private final ModelRenderer north_east2;
    private final ModelRenderer south_east2;
    private final ModelRenderer south_west2;
    private final ModelRenderer west2;
    private final ModelRenderer north_west2;
    private final ModelRenderer north2;
    private final ModelRenderer Wheel_FrontLeft;
    private final ModelRenderer south4;
    private final ModelRenderer west4;
    private final ModelRenderer north_west4;
    private final ModelRenderer south_west4;
    private final ModelRenderer south_east4;
    private final ModelRenderer east4;
    private final ModelRenderer north_east4;
    private final ModelRenderer north4;
    private final ModelRenderer back;
    private final ModelRenderer Wheel_BackRight;
    private final ModelRenderer south;
    private final ModelRenderer east;
    private final ModelRenderer north_east;
    private final ModelRenderer south_east;
    private final ModelRenderer south_west;
    private final ModelRenderer west;
    private final ModelRenderer north_west;
    private final ModelRenderer north;
    private final ModelRenderer Wheel_BackLeft;
    private final ModelRenderer south3;
    private final ModelRenderer west3;
    private final ModelRenderer north_west3;
    private final ModelRenderer south_west3;
    private final ModelRenderer south_east3;
    private final ModelRenderer east3;
    private final ModelRenderer north_east3;
    private final ModelRenderer north3;
    private final ModelRenderer Spare_Wheel;
    private final ModelRenderer south5;
    private final ModelRenderer west5;
    private final ModelRenderer north_west5;
    private final ModelRenderer south_west5;
    private final ModelRenderer south_east5;
    private final ModelRenderer east5;
    private final ModelRenderer north_east5;
    private final ModelRenderer north5;
    private final ModelRenderer Headlights;
    private final ModelRenderer Right;
    private final ModelRenderer Left;
    private final ModelRenderer light_fright;
    private final ModelRenderer light_fleft;
    private final ModelRenderer light_frightlantern;
    private final ModelRenderer light_fleftlantern;
    private final ModelRenderer light_rleft;
    private final ModelRenderer light_rright;
    private final ModelRenderer light_bumperRearLeft;
    private final ModelRenderer light_bumperRearRight;
    private final ModelRenderer Steering;
    private final ModelRenderer steering_assembly_whole;
    private final ModelRenderer steering_stick;
    private final ModelRenderer steering_wheel;
    private final ModelRenderer RearLicensPlate;
    private final ModelRenderer headlights_front_indicators_left;
    private final ModelRenderer headlights_front_indicators_right;
    private final ModelRenderer bb_main;
    

    public BessieModel() {
        textureWidth = 512;
        textureHeight = 512;

        Floor = new ModelRenderer(this);
        Floor.setRotationPoint(0.0F, 24.0F, 0.0F);
        Floor.setTextureOffset(0, 0).addBox(-68.0F, -3.0F, -36.0F, 136.0F, 4.0F, 88.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-52.0F, -19.0F, -32.0F, 104.0F, 4.0F, 48.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-52.0F, -35.0F, 52.0F, 104.0F, 4.0F, 40.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-52.0F, -47.0F, 16.0F, 104.0F, 4.0F, 32.0F, 0.0F, false);
        Floor.setTextureOffset(70, 345).addBox(-52.0F, -48.0F, 17.0F, 104.0F, 1.0F, 30.0F, 0.0F, false);
        Floor.setTextureOffset(68, 347).addBox(-52.0F, -66.0F, 93.0F, 104.0F, 1.0F, 30.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-52.0F, -65.0F, 92.0F, 104.0F, 4.0F, 32.0F, 0.0F, false);
        Floor.setTextureOffset(0, 116).addBox(-56.0F, -3.0F, 52.0F, 112.0F, 4.0F, 60.0F, 0.0F, false);
        Floor.setTextureOffset(0, 170).addBox(-56.0F, -21.7846F, 116.5359F, 112.0F, 4.0F, 12.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-58.0F, -23.0F, 128.0F, 116.0F, 8.0F, 4.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-56.0F, -3.0F, -148.0F, 112.0F, 4.0F, 4.0F, 0.0F, false);
        Floor.setTextureOffset(0, 0).addBox(-36.0F, -3.0F, -144.0F, 72.0F, 4.0F, 108.0F, 0.0F, false);

        bone13 = new ModelRenderer(this);
        bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
        Floor.addChild(bone13);
        setRotationAngle(bone13, 1.0472F, 0.0F, 0.0F);
        bone13.setTextureOffset(155, 415).addBox(-56.0F, 89.4948F, 55.134F, 112.0F, 8.0F, 24.0F, 0.0F, false);

        bone = new ModelRenderer(this);
        bone.setRotationPoint(0.0F, 0.0F, 0.0F);
        Floor.addChild(bone);
        setRotationAngle(bone, 0.0F, -0.0873F, 0.0F);
        bone.setTextureOffset(0, 0).addBox(-48.4134F, -3.0F, -140.3144F, 12.0F, 4.0F, 112.0F, 0.0F, false);

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
        Floor.addChild(bone2);
        setRotationAngle(bone2, 0.0F, 0.0873F, 0.0F);
        bone2.setTextureOffset(0, 0).addBox(36.4134F, -3.0F, -140.3144F, 12.0F, 4.0F, 112.0F, 0.0F, false);

        FrontLicensePlate = new ModelRenderer(this);
        FrontLicensePlate.setRotationPoint(0.0F, 24.0F, 0.0F);
        FrontLicensePlate.setTextureOffset(0, 0).addBox(-17.0F, -3.0F, -150.0F, 1.0F, 10.0F, 2.0F, 0.0F, false);
        FrontLicensePlate.setTextureOffset(0, 0).addBox(-16.0F, -3.0F, -150.0F, 32.0F, 1.0F, 2.0F, 0.0F, false);
        FrontLicensePlate.setTextureOffset(0, 0).addBox(-16.0F, 6.0F, -150.0F, 32.0F, 1.0F, 2.0F, 0.0F, false);
        FrontLicensePlate.setTextureOffset(0, 0).addBox(16.0F, -3.0F, -150.0F, 1.0F, 10.0F, 2.0F, 0.0F, false);
        FrontLicensePlate.setTextureOffset(0, 0).addBox(-16.0F, -2.0F, -149.0F, 32.0F, 8.0F, 1.0F, 0.0F, false);

        RightWall = new ModelRenderer(this);
        RightWall.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        UnderSeatToCarrage2 = new ModelRenderer(this);
        UnderSeatToCarrage2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(UnderSeatToCarrage2);
        UnderSeatToCarrage2.setTextureOffset(0, 0).addBox(-56.0F, -114.0F, 111.0F, 4.0F, 4.0F, 15.0F, 0.0F, false);
        UnderSeatToCarrage2.setTextureOffset(0, 180).addBox(-56.0F, -23.0F, -32.0F, 4.0F, 4.0F, 16.0F, 0.0F, false);
        UnderSeatToCarrage2.setTextureOffset(0, 180).addBox(-56.0F, -31.0F, -32.0F, 4.0F, 8.0F, 5.0F, 0.0F, false);
        UnderSeatToCarrage2.setTextureOffset(0, 182).addBox(-56.0F, -26.0F, -27.0F, 4.0F, 3.0F, 5.0F, 0.0F, false);
        UnderSeatToCarrage2.setTextureOffset(0, 180).addBox(-56.0F, -35.0F, -32.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        right_walls = new ModelRenderer(this);
        right_walls.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(right_walls);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, 16.5914F, -32.1826F, 4.0F, 16.0F, 140.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, 16.5914F, 107.8174F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, 16.5914F, 111.8174F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, 0.5914F, 15.8174F, 4.0F, 16.0F, 112.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, -39.4086F, 15.8174F, 4.0F, 40.0F, 36.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, -4.4086F, 51.8174F, 4.0F, 5.0F, 18.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 180).addBox(-56.2679F, -8.4086F, 51.8174F, 4.0F, 4.0F, 9.0F, 0.0F, false);
        right_walls.setTextureOffset(0, 171).addBox(-56.2679F, -14.4086F, 51.8174F, 4.0F, 6.0F, 4.0F, 0.0F, false);

        back_right = new ModelRenderer(this);
        back_right.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(back_right);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -58.4086F, 89.8174F, 4.0F, 12.0F, 36.0F, 0.0F, false);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -46.4086F, 87.8174F, 4.0F, 47.0F, 40.0F, 0.0F, false);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -66.4086F, 94.8174F, 4.0F, 4.0F, 31.0F, 0.0F, false);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -62.4086F, 91.8174F, 4.0F, 4.0F, 34.0F, 0.0F, false);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -74.4086F, 106.8174F, 4.0F, 4.0F, 19.0F, 0.0F, false);
        back_right.setTextureOffset(0, 182).addBox(-56.2679F, -70.4086F, 97.8174F, 4.0F, 4.0F, 28.0F, 0.0F, false);

        front_right = new ModelRenderer(this);
        front_right.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(front_right);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -51.4086F, 18.8174F, 4.0F, 12.0F, 33.0F, 0.0F, false);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -55.4086F, 20.8174F, 4.0F, 4.0F, 31.0F, 0.0F, false);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -63.4086F, 26.8174F, 4.0F, 4.0F, 25.0F, 0.0F, false);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -59.4086F, 23.8174F, 4.0F, 4.0F, 28.0F, 0.0F, false);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -67.4086F, 35.8174F, 4.0F, 4.0F, 16.0F, 0.0F, false);
        front_right.setTextureOffset(0, 180).addBox(-56.2679F, -71.4086F, 39.8174F, 4.0F, 4.0F, 12.0F, 0.0F, false);

        Engine2 = new ModelRenderer(this);
        Engine2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Engine2);
        

        bone10 = new ModelRenderer(this);
        bone10.setRotationPoint(-8.0F, 0.0F, 0.0F);
        Engine2.addChild(bone10);
        setRotationAngle(bone10, 0.0F, -0.0873F, 0.0F);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -39.0F, -60.3486F, 4.0F, 20.0F, 28.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -39.0F, -76.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -39.0F, -140.3486F, 4.0F, 20.0F, 24.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -39.0F, -92.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -39.0F, -108.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -19.0F, -140.3486F, 4.0F, 16.0F, 108.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-32.0152F, -55.0F, -140.3486F, 4.0F, 16.0F, 108.0F, 0.0F, false);
        bone10.setTextureOffset(0, 178).addBox(-29.1868F, -57.8284F, -140.3486F, 12.0F, 4.0F, 108.0F, 0.0F, false);

        bone21 = new ModelRenderer(this);
        bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone10.addChild(bone21);
        setRotationAngle(bone21, 0.0F, 0.0F, 0.7854F);
        bone21.setTextureOffset(0, 178).addBox(-61.5291F, -20.2527F, -140.3486F, 4.0F, 4.0F, 108.0F, 0.0F, false);

        Fenders2 = new ModelRenderer(this);
        Fenders2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Fenders2);
        

        Front2 = new ModelRenderer(this);
        Front2.setRotationPoint(0.0F, 0.0F, 0.0F);
        Fenders2.addChild(Front2);
        

        bone22 = new ModelRenderer(this);
        bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front2.addChild(bone22);
        setRotationAngle(bone22, 0.3491F, 0.0F, 0.0F);
        bone22.setTextureOffset(404, 299).addBox(-68.0F, -43.373F, -34.171F, 20.0F, 32.0F, 4.0F, 0.0F, false);

        bone23 = new ModelRenderer(this);
        bone23.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front2.addChild(bone23);
        bone23.setTextureOffset(392, 418).addBox(-68.0F, -30.5666F, -26.7205F, 20.0F, 4.0F, 40.0F, 0.0F, false);

        bone24 = new ModelRenderer(this);
        bone24.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front2.addChild(bone24);
        setRotationAngle(bone24, 0.2618F, 0.0F, 0.0F);
        bone24.setTextureOffset(406, 464).addBox(-68.0F, -36.4408F, -37.8988F, 20.0F, 4.0F, 20.0F, 0.0F, false);

        bone25 = new ModelRenderer(this);
        bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front2.addChild(bone25);
        setRotationAngle(bone25, 0.6981F, 0.0F, 0.0F);
        bone25.setTextureOffset(404, 354).addBox(-68.0F, -59.0764F, -17.517F, 20.0F, 8.0F, 4.0F, 0.0F, false);

        bone26 = new ModelRenderer(this);
        bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front2.addChild(bone26);
        setRotationAngle(bone26, 1.0472F, 0.0F, 0.0F);
        bone26.setTextureOffset(409, 381).addBox(-68.0F, -72.1367F, 3.5035F, 20.0F, 12.0F, 4.0F, 0.0F, false);

        Back2 = new ModelRenderer(this);
        Back2.setRotationPoint(0.0F, 0.0F, 36.0F);
        Fenders2.addChild(Back2);
        

        bone62 = new ModelRenderer(this);
        bone62.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back2.addChild(bone62);
        setRotationAngle(bone62, -0.5236F, 0.0F, 0.0F);
        bone62.setTextureOffset(410, 250).addBox(-68.0F, -39.134F, 10.3564F, 20.0F, 32.0F, 4.0F, 0.0F, false);

        bone63 = new ModelRenderer(this);
        bone63.setRotationPoint(0.0F, -12.0F, 72.0F);
        Back2.addChild(bone63);
        bone63.setTextureOffset(389, 160).addBox(-68.0F, -29.7241F, -28.492F, 17.0F, 4.0F, 40.0F, 0.0F, false);

        bone64 = new ModelRenderer(this);
        bone64.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back2.addChild(bone64);
        setRotationAngle(bone64, -0.6981F, 0.0F, 0.0F);
        bone64.setTextureOffset(410, 230).addBox(-68.0F, -48.3378F, 3.4035F, 20.0F, 8.0F, 4.0F, 0.0F, false);

        bone65 = new ModelRenderer(this);
        bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back2.addChild(bone65);
        setRotationAngle(bone65, -0.9599F, 0.0F, 0.0F);
        bone65.setTextureOffset(415, 210).addBox(-68.0F, -59.5716F, -9.2232F, 17.0F, 12.0F, 4.0F, 0.0F, false);

        Edging2 = new ModelRenderer(this);
        Edging2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Edging2);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -87.6413F, -33.6873F, 6.0F, 54.0F, 4.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -110.0F, 48.9492F, 6.0F, 58.0F, 4.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -117.4811F, 125.8983F, 6.0F, 99.0F, 4.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -82.0F, 85.9492F, 6.0F, 41.0F, 4.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -75.0F, 15.0F, 6.0F, 54.0F, 4.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -101.2628F, 27.2465F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -108.2628F, 98.1957F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -110.4811F, 39.9492F, 6.0F, 4.0F, 13.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -117.4811F, 110.8983F, 6.0F, 4.0F, 15.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -21.0F, -12.0F, 6.0F, 4.0F, 31.0F, 0.0F, false);
        Edging2.setTextureOffset(0, 0).addBox(-57.0F, -39.4811F, 70.6365F, 6.0F, 4.0F, 18.0F, 0.0F, false);

        bone66 = new ModelRenderer(this);
        bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging2.addChild(bone66);
        setRotationAngle(bone66, -0.1745F, 0.0F, 0.0F);
        bone66.setTextureOffset(0, 471).addBox(-57.0F, -120.0F, -48.3943F, 6.0F, 39.0F, 4.0F, 0.0F, false);

        bone67 = new ModelRenderer(this);
        bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging2.addChild(bone67);
        setRotationAngle(bone67, -0.1745F, 0.0F, 0.0F);
        bone67.setTextureOffset(0, 0).addBox(-57.0F, -91.4653F, 1.7485F, 6.0F, 15.0F, 4.0F, 0.0F, false);
        bone67.setTextureOffset(0, 0).addBox(-57.0F, -110.6792F, 70.4043F, 6.0F, 15.0F, 4.0F, 0.0F, false);

        bone68 = new ModelRenderer(this);
        bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging2.addChild(bone68);
        setRotationAngle(bone68, -0.6981F, 0.0F, 0.0F);
        bone68.setTextureOffset(0, 0).addBox(-57.0F, -95.0855F, -44.2184F, 6.0F, 15.0F, 4.0F, 0.0F, false);
        bone68.setTextureOffset(0, 0).addBox(-57.0F, -146.0531F, 5.6323F, 6.0F, 15.0F, 4.0F, 0.0F, false);

        bone69 = new ModelRenderer(this);
        bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone68.addChild(bone69);
        setRotationAngle(bone69, -0.1745F, 0.0F, 0.0F);
        bone69.setTextureOffset(0, 0).addBox(-57.0F, -96.0F, -56.6528F, 6.0F, 7.0F, 4.0F, 0.0F, false);
        bone69.setTextureOffset(0, 0).addBox(-57.0F, -154.8497F, -16.4099F, 6.0F, 7.0F, 4.0F, 0.0F, false);

        bone70 = new ModelRenderer(this);
        bone70.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone68.addChild(bone70);
        setRotationAngle(bone70, 0.1745F, 0.0F, 0.0F);
        bone70.setTextureOffset(0, 0).addBox(-57.0F, -115.654F, -20.6435F, 6.0F, 7.0F, 4.0F, 0.0F, false);
        bone70.setTextureOffset(0, 0).addBox(-57.0F, -157.1908F, 37.3002F, 6.0F, 7.0F, 4.0F, 0.0F, false);

        bone71 = new ModelRenderer(this);
        bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging2.addChild(bone71);
        setRotationAngle(bone71, -0.4363F, 0.0F, 0.0F);
        bone71.setTextureOffset(0, 0).addBox(-57.0F, -14.3358F, -32.0602F, 6.0F, 4.0F, 14.0F, 0.0F, false);
        bone71.setTextureOffset(0, 0).addBox(-57.0F, -66.009F, 35.0235F, 6.0F, 4.0F, 14.0F, 0.0F, false);

        bone72 = new ModelRenderer(this);
        bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging2.addChild(bone72);
        setRotationAngle(bone72, -0.8727F, 0.0F, 0.0F);
        bone72.setTextureOffset(0, 0).addBox(-57.0F, 0.1818F, -47.4245F, 6.0F, 4.0F, 14.0F, 0.0F, false);
        bone72.setTextureOffset(6, 0).addBox(-57.0F, -75.0008F, -8.4641F, 6.0F, 4.0F, 14.0F, 0.0F, false);

        LeftWall = new ModelRenderer(this);
        LeftWall.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        UnderSeatToCarrage = new ModelRenderer(this);
        UnderSeatToCarrage.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(UnderSeatToCarrage);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -94.0F, 90.0F, 4.0F, 12.0F, 36.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -98.0F, 92.0F, 4.0F, 4.0F, 34.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -102.0F, 95.0F, 4.0F, 4.0F, 31.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -106.0F, 98.0F, 4.0F, 4.0F, 28.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -110.0F, 107.0F, 4.0F, 4.0F, 19.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 180).addBox(52.0F, -114.0F, 111.0F, 4.0F, 4.0F, 15.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 178).addBox(52.0F, -23.0F, -32.0F, 4.0F, 4.0F, 16.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 178).addBox(52.0F, -31.0F, -32.0F, 4.0F, 8.0F, 5.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 178).addBox(52.0F, -26.0F, -27.0F, 4.0F, 3.0F, 5.0F, 0.0F, false);
        UnderSeatToCarrage.setTextureOffset(0, 182).addBox(52.0F, -35.0F, -32.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        front_left2 = new ModelRenderer(this);
        front_left2.setRotationPoint(-0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage.addChild(front_left2);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, 16.5914F, -32.1826F, 4.0F, 16.0F, 140.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, 16.5914F, 107.8174F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, 16.5914F, 111.8174F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, 0.5914F, 15.8174F, 4.0F, 16.0F, 112.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, -39.4086F, 15.8174F, 4.0F, 40.0F, 36.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, -4.4086F, 51.8174F, 4.0F, 5.0F, 18.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, -8.4086F, 51.8174F, 4.0F, 4.0F, 9.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 171).addBox(52.2679F, -14.4086F, 51.8174F, 4.0F, 6.0F, 4.0F, 0.0F, false);
        front_left2.setTextureOffset(0, 180).addBox(52.2679F, -46.4086F, 87.8174F, 4.0F, 47.0F, 40.0F, 0.0F, false);

        front_left = new ModelRenderer(this);
        front_left.setRotationPoint(-0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage.addChild(front_left);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -51.4086F, 18.8174F, 4.0F, 12.0F, 33.0F, 0.0F, false);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -55.4086F, 20.8174F, 4.0F, 4.0F, 31.0F, 0.0F, false);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -59.4086F, 23.8174F, 4.0F, 4.0F, 28.0F, 0.0F, false);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -63.4086F, 26.8174F, 4.0F, 4.0F, 25.0F, 0.0F, false);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -67.4086F, 35.8174F, 4.0F, 4.0F, 16.0F, 0.0F, false);
        front_left.setTextureOffset(0, 180).addBox(52.2679F, -71.4086F, 39.8174F, 4.0F, 4.0F, 12.0F, 0.0F, false);

        Engine = new ModelRenderer(this);
        Engine.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Engine);
        

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(8.0F, 0.0F, 0.0F);
        Engine.addChild(bone3);
        setRotationAngle(bone3, 0.0F, 0.0873F, 0.0F);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -39.0F, -60.3486F, 4.0F, 20.0F, 28.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -39.0F, -76.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -39.0F, -140.3486F, 4.0F, 20.0F, 24.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -39.0F, -92.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -39.0F, -108.3486F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -19.0F, -140.3486F, 4.0F, 16.0F, 108.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(28.0152F, -55.0F, -140.3486F, 4.0F, 16.0F, 108.0F, 0.0F, false);
        bone3.setTextureOffset(0, 178).addBox(17.1868F, -57.8284F, -140.3486F, 12.0F, 4.0F, 108.0F, 0.0F, false);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone3.addChild(bone4);
        setRotationAngle(bone4, 0.0F, 0.0F, -0.7854F);
        bone4.setTextureOffset(0, 178).addBox(57.5291F, -20.2527F, -140.3486F, 4.0F, 4.0F, 108.0F, 0.0F, false);

        Fenders = new ModelRenderer(this);
        Fenders.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Fenders);
        

        Front = new ModelRenderer(this);
        Front.setRotationPoint(0.0F, 0.0F, 0.0F);
        Fenders.addChild(Front);
        

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front.addChild(bone5);
        setRotationAngle(bone5, 0.3491F, 0.0F, 0.0F);
        bone5.setTextureOffset(404, 299).addBox(48.0F, -43.373F, -34.171F, 20.0F, 32.0F, 4.0F, 0.0F, false);

        bone8 = new ModelRenderer(this);
        bone8.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front.addChild(bone8);
        bone8.setTextureOffset(392, 418).addBox(48.0F, -30.5666F, -26.7205F, 20.0F, 4.0F, 40.0F, 0.0F, false);

        bone9 = new ModelRenderer(this);
        bone9.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front.addChild(bone9);
        setRotationAngle(bone9, 0.2618F, 0.0F, 0.0F);
        bone9.setTextureOffset(406, 464).addBox(48.0F, -36.4408F, -37.8988F, 20.0F, 4.0F, 20.0F, 0.0F, false);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front.addChild(bone6);
        setRotationAngle(bone6, 0.6981F, 0.0F, 0.0F);
        bone6.setTextureOffset(404, 354).addBox(48.0F, -59.0764F, -17.517F, 20.0F, 8.0F, 4.0F, 0.0F, false);

        bone7 = new ModelRenderer(this);
        bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
        Front.addChild(bone7);
        setRotationAngle(bone7, 1.0472F, 0.0F, 0.0F);
        bone7.setTextureOffset(409, 381).addBox(48.0F, -72.1367F, 3.5035F, 20.0F, 12.0F, 4.0F, 0.0F, false);

        Back = new ModelRenderer(this);
        Back.setRotationPoint(0.0F, 0.0F, 36.0F);
        Fenders.addChild(Back);
        

        bone11 = new ModelRenderer(this);
        bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back.addChild(bone11);
        setRotationAngle(bone11, -0.5236F, 0.0F, 0.0F);
        bone11.setTextureOffset(410, 250).addBox(48.0F, -39.134F, 10.3564F, 20.0F, 32.0F, 4.0F, 0.0F, false);

        bone12 = new ModelRenderer(this);
        bone12.setRotationPoint(0.0F, -12.0F, 72.0F);
        Back.addChild(bone12);
        bone12.setTextureOffset(389, 160).addBox(51.0F, -29.7241F, -28.492F, 17.0F, 4.0F, 40.0F, 0.0F, false);

        bone14 = new ModelRenderer(this);
        bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back.addChild(bone14);
        setRotationAngle(bone14, -0.6981F, 0.0F, 0.0F);
        bone14.setTextureOffset(410, 230).addBox(48.0F, -48.3378F, 3.4035F, 20.0F, 8.0F, 4.0F, 0.0F, false);

        bone15 = new ModelRenderer(this);
        bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
        Back.addChild(bone15);
        setRotationAngle(bone15, -0.9599F, 0.0F, 0.0F);
        bone15.setTextureOffset(415, 210).addBox(51.0F, -59.5716F, -9.2232F, 17.0F, 12.0F, 4.0F, 0.0F, false);

        Edging = new ModelRenderer(this);
        Edging.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Edging);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -87.6413F, -33.6873F, 6.0F, 54.0F, 4.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -110.0F, 48.9492F, 6.0F, 58.0F, 4.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -117.4811F, 125.8983F, 6.0F, 99.0F, 4.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -82.0F, 85.9492F, 6.0F, 41.0F, 4.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -75.0F, 15.0F, 6.0F, 54.0F, 4.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -101.2628F, 27.2465F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -108.2628F, 98.1957F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -110.4811F, 39.9492F, 6.0F, 4.0F, 13.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -117.4811F, 110.8983F, 6.0F, 4.0F, 15.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -21.0F, -12.0F, 6.0F, 4.0F, 31.0F, 0.0F, false);
        Edging.setTextureOffset(0, 0).addBox(51.0F, -39.4811F, 70.6365F, 6.0F, 4.0F, 18.0F, 0.0F, false);

        bone16 = new ModelRenderer(this);
        bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging.addChild(bone16);
        setRotationAngle(bone16, -0.1745F, 0.0F, 0.0F);
        bone16.setTextureOffset(0, 471).addBox(51.0F, -120.0F, -48.3943F, 6.0F, 39.0F, 4.0F, 0.0F, false);

        bone17 = new ModelRenderer(this);
        bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging.addChild(bone17);
        setRotationAngle(bone17, -0.1745F, 0.0F, 0.0F);
        bone17.setTextureOffset(0, 0).addBox(51.0F, -91.4653F, 1.7485F, 6.0F, 15.0F, 4.0F, 0.0F, false);
        bone17.setTextureOffset(0, 0).addBox(51.0F, -110.6792F, 70.4043F, 6.0F, 15.0F, 4.0F, 0.0F, false);

        bone19 = new ModelRenderer(this);
        bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging.addChild(bone19);
        setRotationAngle(bone19, -0.6981F, 0.0F, 0.0F);
        bone19.setTextureOffset(0, 0).addBox(51.0F, -95.0855F, -44.2184F, 6.0F, 15.0F, 4.0F, 0.0F, false);
        bone19.setTextureOffset(0, 0).addBox(51.0F, -146.0531F, 5.6323F, 6.0F, 15.0F, 4.0F, 0.0F, false);

        bone18 = new ModelRenderer(this);
        bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone19.addChild(bone18);
        setRotationAngle(bone18, -0.1745F, 0.0F, 0.0F);
        bone18.setTextureOffset(0, 0).addBox(51.0F, -96.0F, -56.6528F, 6.0F, 7.0F, 4.0F, 0.0F, false);
        bone18.setTextureOffset(0, 0).addBox(51.0F, -154.8497F, -16.4099F, 6.0F, 7.0F, 4.0F, 0.0F, false);

        bone20 = new ModelRenderer(this);
        bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone19.addChild(bone20);
        setRotationAngle(bone20, 0.1745F, 0.0F, 0.0F);
        bone20.setTextureOffset(0, 0).addBox(51.0F, -115.654F, -20.6435F, 6.0F, 7.0F, 4.0F, 0.0F, false);
        bone20.setTextureOffset(0, 0).addBox(51.0F, -157.1908F, 37.3002F, 6.0F, 7.0F, 4.0F, 0.0F, false);

        bone60 = new ModelRenderer(this);
        bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging.addChild(bone60);
        setRotationAngle(bone60, -0.4363F, 0.0F, 0.0F);
        bone60.setTextureOffset(0, 0).addBox(51.0F, -14.3358F, -32.0602F, 6.0F, 4.0F, 14.0F, 0.0F, false);
        bone60.setTextureOffset(0, 0).addBox(51.0F, -66.009F, 35.0235F, 6.0F, 4.0F, 14.0F, 0.0F, false);

        bone61 = new ModelRenderer(this);
        bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
        Edging.addChild(bone61);
        setRotationAngle(bone61, -0.8727F, 0.0F, 0.0F);
        bone61.setTextureOffset(0, 0).addBox(51.0F, 0.1818F, -47.4245F, 6.0F, 4.0F, 14.0F, 0.0F, false);
        bone61.setTextureOffset(0, 0).addBox(51.0F, -75.0008F, -8.4641F, 6.0F, 4.0F, 14.0F, 0.0F, false);

        CrossBeams = new ModelRenderer(this);
        CrossBeams.setRotationPoint(0.0F, 24.0F, 0.0F);
        CrossBeams.setTextureOffset(0, 0).addBox(-28.0F, -55.0F, -136.0F, 56.0F, 52.0F, 88.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 180).addBox(-56.0F, -75.0F, -36.0F, 112.0F, 72.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 0).addBox(-24.0F, -55.0F, -141.2F, 48.0F, 52.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 180).addBox(-52.0F, -43.0F, 16.0F, 104.0F, 40.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 180).addBox(-52.0F, -61.0F, 92.0F, 104.0F, 54.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 180).addBox(-52.0F, -109.0F, 48.0F, 104.0F, 105.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 0).addBox(-52.0F, -110.5F, 48.0F, 104.0F, 2.0F, 5.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 0).addBox(-52.0F, -109.0F, 47.0F, 104.0F, 106.0F, 1.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 0).addBox(-52.0F, -115.0F, 123.0F, 104.0F, 94.0F, 1.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 196).addBox(-52.0F, -115.0F, 124.0F, 104.0F, 92.0F, 4.0F, 0.0F, false);
        CrossBeams.setTextureOffset(0, 0).addBox(-52.0F, -117.5F, 124.0F, 104.0F, 3.0F, 5.0F, 0.0F, false);
        CrossBeams.setTextureOffset(82, 361).addBox(40.0F, -8.0F, 108.0F, 7.0F, 7.0F, 21.0F, 0.0F, false);

        WindowFrame = new ModelRenderer(this);
        WindowFrame.setRotationPoint(0.0F, 0.0F, 0.0F);
        CrossBeams.addChild(WindowFrame);
        setRotationAngle(WindowFrame, -0.1745F, 0.0F, 0.0F);
        WindowFrame.setTextureOffset(0, 458).addBox(-56.0F, -119.6092F, -48.4767F, 8.0F, 52.0F, 4.0F, 0.0F, false);
        WindowFrame.setTextureOffset(0, 458).addBox(48.0F, -119.6092F, -48.4767F, 8.0F, 52.0F, 4.0F, 0.0F, false);
        WindowFrame.setTextureOffset(0, 502).addBox(-48.0F, -119.6092F, -48.4767F, 96.0F, 8.0F, 4.0F, 0.0F, false);

        Supports = new ModelRenderer(this);
        Supports.setRotationPoint(0.0F, 0.0F, 0.0F);
        WindowFrame.addChild(Supports);
        setRotationAngle(Supports, -0.3491F, 0.0F, 0.0F);
        Supports.setTextureOffset(0, 410).addBox(48.0F, -95.8159F, -86.462F, 8.0F, 100.0F, 4.0F, 0.0F, false);
        Supports.setTextureOffset(0, 410).addBox(-56.0F, -95.8159F, -86.462F, 8.0F, 100.0F, 4.0F, 0.0F, false);

        Axles = new ModelRenderer(this);
        Axles.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        front = new ModelRenderer(this);
        front.setRotationPoint(0.0F, -9.0F, -90.0F);
        Axles.addChild(front);
        front.setTextureOffset(0, 0).addBox(-64.0F, -2.0F, -2.0F, 128.0F, 4.0F, 4.0F, 0.0F, false);

        Wheel_FrontRight = new ModelRenderer(this);
        Wheel_FrontRight.setRotationPoint(63.0F, 0.0F, 0.0F);
        front.addChild(Wheel_FrontRight);
        

        south2 = new ModelRenderer(this);
        south2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(south2);
        south2.setTextureOffset(0, 180).addBox(-4.0F, 1.0F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south2.setTextureOffset(0, 187).addBox(-4.0F, -4.0F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        south2.setTextureOffset(0, 0).addBox(-4.0F, 21.0F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        east2 = new ModelRenderer(this);
        east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(east2);
        setRotationAngle(east2, 1.5708F, 0.0F, 0.0F);
        east2.setTextureOffset(0, 180).addBox(-4.0F, 4.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east2.setTextureOffset(0, 185).addBox(-4.0F, -0.0294F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east2.setTextureOffset(0, 0).addBox(-4.0F, 24.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_east2 = new ModelRenderer(this);
        north_east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(north_east2);
        setRotationAngle(north_east2, 2.3562F, 0.0F, 0.0F);
        north_east2.setTextureOffset(0, 180).addBox(-4.0F, 7.7782F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east2.setTextureOffset(0, 187).addBox(-4.0F, 3.7782F, -1.1924F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_east2.setTextureOffset(0, 0).addBox(-4.0F, 27.7782F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_east2 = new ModelRenderer(this);
        south_east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(south_east2);
        setRotationAngle(south_east2, 0.7854F, 0.0F, 0.0F);
        south_east2.setTextureOffset(0, 180).addBox(-4.0F, 2.163F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east2.setTextureOffset(0, 187).addBox(-4.0F, 0.163F, -1.1924F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_east2.setTextureOffset(0, 0).addBox(-4.0F, 22.163F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_west2 = new ModelRenderer(this);
        south_west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(south_west2);
        setRotationAngle(south_west2, -0.7854F, 0.0F, 0.0F);
        south_west2.setTextureOffset(0, 180).addBox(-4.0F, 2.163F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west2.setTextureOffset(572, 1725).addBox(-4.0F, 0.163F, -6.8076F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west2.setTextureOffset(0, 0).addBox(-4.0F, 22.163F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        west2 = new ModelRenderer(this);
        west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(west2);
        setRotationAngle(west2, -1.5708F, 0.0F, 0.0F);
        west2.setTextureOffset(0, 180).addBox(-4.0F, 4.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west2.setTextureOffset(0, 187).addBox(-4.0F, -0.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        west2.setTextureOffset(0, 0).addBox(-4.0F, 24.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_west2 = new ModelRenderer(this);
        north_west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(north_west2);
        setRotationAngle(north_west2, -2.3562F, 0.0F, 0.0F);
        north_west2.setTextureOffset(0, 180).addBox(-4.0F, 7.7782F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west2.setTextureOffset(0, 187).addBox(-4.0F, 3.7782F, -6.8076F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_west2.setTextureOffset(0, 0).addBox(-4.0F, 27.7782F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north2 = new ModelRenderer(this);
        north2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(north2);
        setRotationAngle(north2, 3.1416F, 0.0F, 0.0F);
        north2.setTextureOffset(0, 180).addBox(-4.0F, 8.9411F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north2.setTextureOffset(0, 178).addBox(-4.0F, 3.9411F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north2.setTextureOffset(0, 0).addBox(-4.0F, 28.9411F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        Wheel_FrontLeft = new ModelRenderer(this);
        Wheel_FrontLeft.setRotationPoint(-63.0F, 0.0F, 0.0F);
        front.addChild(Wheel_FrontLeft);
        

        south4 = new ModelRenderer(this);
        south4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(south4);
        south4.setTextureOffset(0, 182).addBox(0.0F, 1.0F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south4.setTextureOffset(0, 178).addBox(0.0F, -4.0F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        south4.setTextureOffset(0, 0).addBox(-2.0F, 21.0F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        west4 = new ModelRenderer(this);
        west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(west4);
        setRotationAngle(west4, 1.5708F, 0.0F, 0.0F);
        west4.setTextureOffset(0, 182).addBox(0.0F, 4.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west4.setTextureOffset(0, 187).addBox(0.0F, -0.0294F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        west4.setTextureOffset(0, 0).addBox(-2.0F, 24.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_west4 = new ModelRenderer(this);
        north_west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(north_west4);
        setRotationAngle(north_west4, 2.3562F, 0.0F, 0.0F);
        north_west4.setTextureOffset(0, 182).addBox(0.0F, 7.7782F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west4.setTextureOffset(0, 182).addBox(0.0F, 3.7782F, -1.1924F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_west4.setTextureOffset(0, 0).addBox(-2.0F, 27.7782F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_west4 = new ModelRenderer(this);
        south_west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(south_west4);
        setRotationAngle(south_west4, 0.7854F, 0.0F, 0.0F);
        south_west4.setTextureOffset(0, 182).addBox(0.0F, 2.163F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west4.setTextureOffset(0, 182).addBox(0.0F, 0.163F, -1.1924F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west4.setTextureOffset(0, 0).addBox(-2.0F, 22.163F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_east4 = new ModelRenderer(this);
        south_east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(south_east4);
        setRotationAngle(south_east4, -0.7854F, 0.0F, 0.0F);
        south_east4.setTextureOffset(0, 182).addBox(0.0F, 2.163F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east4.setTextureOffset(0, 182).addBox(0.0F, 0.163F, -6.8076F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_east4.setTextureOffset(0, 0).addBox(-2.0F, 22.163F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        east4 = new ModelRenderer(this);
        east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(east4);
        setRotationAngle(east4, -1.5708F, 0.0F, 0.0F);
        east4.setTextureOffset(0, 182).addBox(0.0F, 4.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east4.setTextureOffset(0, 182).addBox(0.0F, -0.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east4.setTextureOffset(0, 0).addBox(-2.0F, 24.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_east4 = new ModelRenderer(this);
        north_east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(north_east4);
        setRotationAngle(north_east4, -2.3562F, 0.0F, 0.0F);
        north_east4.setTextureOffset(0, 182).addBox(0.0F, 7.7782F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east4.setTextureOffset(0, 182).addBox(0.0F, 3.7782F, -6.8076F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_east4.setTextureOffset(0, 0).addBox(-2.0F, 27.7782F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north4 = new ModelRenderer(this);
        north4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(north4);
        setRotationAngle(north4, 3.1416F, 0.0F, 0.0F);
        north4.setTextureOffset(0, 182).addBox(0.0F, 8.9411F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north4.setTextureOffset(0, 182).addBox(0.0F, 3.9411F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north4.setTextureOffset(0, 0).addBox(-2.0F, 28.9411F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        back = new ModelRenderer(this);
        back.setRotationPoint(0.0F, -9.0F, 94.0F);
        Axles.addChild(back);
        back.setTextureOffset(0, 0).addBox(-64.0F, -2.0F, -2.0F, 128.0F, 4.0F, 4.0F, 0.0F, false);

        Wheel_BackRight = new ModelRenderer(this);
        Wheel_BackRight.setRotationPoint(63.0F, 0.0F, 0.0F);
        back.addChild(Wheel_BackRight);
        

        south = new ModelRenderer(this);
        south.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(south);
        south.setTextureOffset(0, 180).addBox(-4.0F, 1.0F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south.setTextureOffset(0, 180).addBox(-4.0F, -4.0F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        south.setTextureOffset(0, 0).addBox(-4.0F, 21.0F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        east = new ModelRenderer(this);
        east.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(east);
        setRotationAngle(east, 1.5708F, 0.0F, 0.0F);
        east.setTextureOffset(0, 180).addBox(-4.0F, 4.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east.setTextureOffset(0, 180).addBox(-4.0F, -0.0294F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east.setTextureOffset(0, 0).addBox(-4.0F, 24.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_east = new ModelRenderer(this);
        north_east.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(north_east);
        setRotationAngle(north_east, 2.3562F, 0.0F, 0.0F);
        north_east.setTextureOffset(0, 180).addBox(-4.0F, 7.7782F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east.setTextureOffset(0, 180).addBox(-4.0F, 3.7782F, -1.1924F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_east.setTextureOffset(0, 0).addBox(-4.0F, 27.7782F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_east = new ModelRenderer(this);
        south_east.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(south_east);
        setRotationAngle(south_east, 0.7854F, 0.0F, 0.0F);
        south_east.setTextureOffset(0, 180).addBox(-4.0F, 2.163F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east.setTextureOffset(596, 1725).addBox(-4.0F, 0.163F, -1.1924F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_east.setTextureOffset(0, 0).addBox(-4.0F, 22.163F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_west = new ModelRenderer(this);
        south_west.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(south_west);
        setRotationAngle(south_west, -0.7854F, 0.0F, 0.0F);
        south_west.setTextureOffset(0, 180).addBox(-4.0F, 2.163F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west.setTextureOffset(0, 180).addBox(-4.0F, 0.163F, -6.8076F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west.setTextureOffset(0, 0).addBox(-4.0F, 22.163F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        west = new ModelRenderer(this);
        west.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(west);
        setRotationAngle(west, -1.5708F, 0.0F, 0.0F);
        west.setTextureOffset(0, 180).addBox(-4.0F, 4.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west.setTextureOffset(0, 194).addBox(-4.0F, -0.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        west.setTextureOffset(0, 0).addBox(-4.0F, 24.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_west = new ModelRenderer(this);
        north_west.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(north_west);
        setRotationAngle(north_west, -2.3562F, 0.0F, 0.0F);
        north_west.setTextureOffset(0, 180).addBox(-4.0F, 7.7782F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west.setTextureOffset(0, 180).addBox(-4.0F, 3.7782F, -6.8076F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_west.setTextureOffset(0, 0).addBox(-4.0F, 27.7782F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north = new ModelRenderer(this);
        north.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(north);
        setRotationAngle(north, 3.1416F, 0.0F, 0.0F);
        north.setTextureOffset(0, 180).addBox(-4.0F, 8.9411F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north.setTextureOffset(0, 182).addBox(-4.0F, 3.9411F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north.setTextureOffset(0, 0).addBox(-4.0F, 28.9411F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        Wheel_BackLeft = new ModelRenderer(this);
        Wheel_BackLeft.setRotationPoint(-63.0F, 0.0F, 0.0F);
        back.addChild(Wheel_BackLeft);
        

        south3 = new ModelRenderer(this);
        south3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(south3);
        south3.setTextureOffset(0, 180).addBox(0.0F, 1.0F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south3.setTextureOffset(0, 180).addBox(0.0F, -4.0F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        south3.setTextureOffset(0, 0).addBox(-2.0F, 21.0F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        west3 = new ModelRenderer(this);
        west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(west3);
        setRotationAngle(west3, 1.5708F, 0.0F, 0.0F);
        west3.setTextureOffset(0, 180).addBox(0.0F, 4.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west3.setTextureOffset(0, 180).addBox(0.0F, -0.0294F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        west3.setTextureOffset(0, 0).addBox(-2.0F, 24.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_west3 = new ModelRenderer(this);
        north_west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(north_west3);
        setRotationAngle(north_west3, 2.3562F, 0.0F, 0.0F);
        north_west3.setTextureOffset(0, 180).addBox(0.0F, 7.7782F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west3.setTextureOffset(0, 180).addBox(0.0F, 3.7782F, -1.1924F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_west3.setTextureOffset(0, 0).addBox(-2.0F, 27.7782F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_west3 = new ModelRenderer(this);
        south_west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(south_west3);
        setRotationAngle(south_west3, 0.7854F, 0.0F, 0.0F);
        south_west3.setTextureOffset(0, 180).addBox(0.0F, 2.163F, -1.1924F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west3.setTextureOffset(500, 1725).addBox(0.0F, 0.163F, -1.1924F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west3.setTextureOffset(0, 0).addBox(-2.0F, 22.163F, -9.1924F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_east3 = new ModelRenderer(this);
        south_east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(south_east3);
        setRotationAngle(south_east3, -0.7854F, 0.0F, 0.0F);
        south_east3.setTextureOffset(0, 180).addBox(0.0F, 2.163F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east3.setTextureOffset(0, 180).addBox(0.0F, 0.163F, -6.8076F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_east3.setTextureOffset(0, 0).addBox(-2.0F, 22.163F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        east3 = new ModelRenderer(this);
        east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(east3);
        setRotationAngle(east3, -1.5708F, 0.0F, 0.0F);
        east3.setTextureOffset(0, 180).addBox(0.0F, 4.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east3.setTextureOffset(0, 180).addBox(0.0F, -0.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east3.setTextureOffset(0, 0).addBox(-2.0F, 24.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_east3 = new ModelRenderer(this);
        north_east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(north_east3);
        setRotationAngle(north_east3, -2.3562F, 0.0F, 0.0F);
        north_east3.setTextureOffset(0, 180).addBox(0.0F, 7.7782F, -6.8076F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east3.setTextureOffset(0, 185).addBox(0.0F, 3.7782F, -6.8076F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        north_east3.setTextureOffset(0, 0).addBox(-2.0F, 27.7782F, -14.8076F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north3 = new ModelRenderer(this);
        north3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(north3);
        setRotationAngle(north3, 3.1416F, 0.0F, 0.0F);
        north3.setTextureOffset(0, 180).addBox(0.0F, 8.9411F, -4.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north3.setTextureOffset(2, 176).addBox(0.0F, 3.9411F, -4.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north3.setTextureOffset(0, 0).addBox(-2.0F, 28.9411F, -12.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        Spare_Wheel = new ModelRenderer(this);
        Spare_Wheel.setRotationPoint(-3.0F, -46.0F, 132.0F);
        setRotationAngle(Spare_Wheel, 0.0F, 1.5708F, 0.0F);
        

        south5 = new ModelRenderer(this);
        south5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(south5);
        south5.setTextureOffset(434, 114).addBox(0.0F, 1.0F, -1.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south5.setTextureOffset(434, 114).addBox(0.0F, 1.0F, -1.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south5.setTextureOffset(0, 180).addBox(0.0F, -4.0F, -1.0F, 4.0F, 9.0F, 8.0F, 0.0F, true);
        south5.setTextureOffset(0, 180).addBox(0.0F, -4.0F, -1.0F, 4.0F, 9.0F, 8.0F, 0.0F, true);
        south5.setTextureOffset(0, 0).addBox(-2.0F, 21.0F, -9.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        south5.setTextureOffset(0, 0).addBox(-2.0F, 21.0F, -9.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        west5 = new ModelRenderer(this);
        west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(west5);
        setRotationAngle(west5, 1.5708F, 0.0F, 0.0F);
        west5.setTextureOffset(390, 160).addBox(0.0F, 7.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west5.setTextureOffset(390, 160).addBox(0.0F, 7.9706F, -0.0294F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        west5.setTextureOffset(0, 180).addBox(0.0F, 2.9706F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, true);
        west5.setTextureOffset(0, 180).addBox(0.0F, 2.9706F, -0.0294F, 4.0F, 9.0F, 8.0F, 0.0F, true);
        west5.setTextureOffset(0, 0).addBox(-2.0F, 27.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        west5.setTextureOffset(0, 0).addBox(-2.0F, 27.9706F, -8.0294F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_west5 = new ModelRenderer(this);
        north_west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(north_west5);
        setRotationAngle(north_west5, 2.3562F, 0.0F, 0.0F);
        north_west5.setTextureOffset(477, 114).addBox(0.0F, 9.8995F, -3.3137F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west5.setTextureOffset(477, 114).addBox(0.0F, 9.8995F, -3.3137F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_west5.setTextureOffset(0, 180).addBox(0.0F, 5.8995F, -3.3137F, 4.0F, 8.0F, 8.0F, 0.0F, true);
        north_west5.setTextureOffset(0, 180).addBox(0.0F, 5.8995F, -3.3137F, 4.0F, 8.0F, 8.0F, 0.0F, true);
        north_west5.setTextureOffset(0, 0).addBox(-2.0F, 29.8995F, -11.3137F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        north_west5.setTextureOffset(0, 0).addBox(-2.0F, 29.8995F, -11.3137F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_west5 = new ModelRenderer(this);
        south_west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(south_west5);
        setRotationAngle(south_west5, 0.7854F, 0.0F, 0.0F);
        south_west5.setTextureOffset(395, 115).addBox(0.0F, 4.2843F, 0.9289F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west5.setTextureOffset(395, 115).addBox(0.0F, 4.2843F, 0.9289F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_west5.setTextureOffset(452, 1725).addBox(0.0F, 2.2843F, 0.9289F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west5.setTextureOffset(452, 1725).addBox(0.0F, 2.2843F, 0.9289F, 4.0F, 6.0F, 8.0F, 0.0F, false);
        south_west5.setTextureOffset(0, 0).addBox(-2.0F, 24.2843F, -7.0711F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        south_west5.setTextureOffset(0, 0).addBox(-2.0F, 24.2843F, -7.0711F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        south_east5 = new ModelRenderer(this);
        south_east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(south_east5);
        setRotationAngle(south_east5, -0.7854F, 0.0F, 0.0F);
        south_east5.setTextureOffset(477, 114).addBox(0.0F, 0.0416F, -4.6863F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east5.setTextureOffset(477, 114).addBox(0.0F, 0.0416F, -4.6863F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        south_east5.setTextureOffset(0, 180).addBox(0.0F, -1.9584F, -4.6863F, 4.0F, 6.0F, 8.0F, 0.0F, true);
        south_east5.setTextureOffset(0, 180).addBox(0.0F, -1.9584F, -4.6863F, 4.0F, 6.0F, 8.0F, 0.0F, true);
        south_east5.setTextureOffset(0, 0).addBox(-2.0F, 20.0416F, -12.6863F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        south_east5.setTextureOffset(0, 0).addBox(-2.0F, 20.0416F, -12.6863F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        east5 = new ModelRenderer(this);
        east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(east5);
        setRotationAngle(east5, -1.5708F, 0.0F, 0.0F);
        east5.setTextureOffset(390, 160).addBox(0.0F, 1.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east5.setTextureOffset(390, 160).addBox(0.0F, 1.9706F, -7.9706F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        east5.setTextureOffset(0, 178).addBox(0.0F, -3.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east5.setTextureOffset(0, 178).addBox(0.0F, -3.0294F, -7.9706F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        east5.setTextureOffset(0, 0).addBox(-2.0F, 21.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        east5.setTextureOffset(0, 0).addBox(-2.0F, 21.9706F, -15.9706F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north_east5 = new ModelRenderer(this);
        north_east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(north_east5);
        setRotationAngle(north_east5, -2.3562F, 0.0F, 0.0F);
        north_east5.setTextureOffset(395, 115).addBox(0.0F, 5.6569F, -8.9289F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east5.setTextureOffset(395, 115).addBox(0.0F, 5.6569F, -8.9289F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north_east5.setTextureOffset(0, 180).addBox(0.0F, 1.6569F, -8.9289F, 4.0F, 8.0F, 8.0F, 0.0F, true);
        north_east5.setTextureOffset(0, 180).addBox(0.0F, 1.6569F, -8.9289F, 4.0F, 8.0F, 8.0F, 0.0F, true);
        north_east5.setTextureOffset(0, 0).addBox(-2.0F, 25.6569F, -16.9289F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        north_east5.setTextureOffset(0, 0).addBox(-2.0F, 25.6569F, -16.9289F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        north5 = new ModelRenderer(this);
        north5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(north5);
        setRotationAngle(north5, 3.1416F, 0.0F, 0.0F);
        north5.setTextureOffset(434, 114).addBox(0.0F, 8.9411F, -7.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north5.setTextureOffset(434, 114).addBox(0.0F, 8.9411F, -7.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
        north5.setTextureOffset(0, 180).addBox(0.0F, 3.9411F, -7.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north5.setTextureOffset(0, 180).addBox(0.0F, 3.9411F, -7.0F, 4.0F, 9.0F, 8.0F, 0.0F, false);
        north5.setTextureOffset(0, 0).addBox(-2.0F, 28.9411F, -15.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);
        north5.setTextureOffset(0, 0).addBox(-2.0F, 28.9411F, -15.0F, 6.0F, 4.0F, 24.0F, 0.0F, false);

        Headlights = new ModelRenderer(this);
        Headlights.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        Right = new ModelRenderer(this);
        Right.setRotationPoint(0.0F, 0.0F, 0.0F);
        Headlights.addChild(Right);
        setRotationAngle(Right, 0.0F, 0.0F, -0.0873F);
        Right.setTextureOffset(0, 0).addBox(-33.0F, -38.0F, -144.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-37.0F, -48.0F, -143.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-37.0F, -48.0F, -144.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-36.0F, -39.0F, -144.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-36.0F, -48.0F, -144.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-28.0F, -48.0F, -144.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-37.8097F, -73.1663F, -37.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-37.8097F, -73.1663F, -38.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-37.3097F, -64.6663F, -38.5F, 9.0F, 2.0F, 2.0F, -0.5F, false);
        Right.setTextureOffset(0, 352).addBox(-36.8097F, -73.1663F, -38.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Right.setTextureOffset(0, 352).addBox(-28.8097F, -73.1663F, -38.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);

        Left = new ModelRenderer(this);
        Left.setRotationPoint(0.0F, 0.0F, 0.0F);
        Headlights.addChild(Left);
        setRotationAngle(Left, 0.0F, 0.0F, 0.0873F);
        Left.setTextureOffset(0, 0).addBox(31.0F, -38.0F, -144.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(36.0F, -48.0F, -144.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(28.0F, -39.0F, -144.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(28.0F, -48.0F, -144.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(27.0F, -48.0F, -144.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(27.0F, -48.0F, -143.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(24.8211F, -72.9049F, -37.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(33.8211F, -72.9049F, -38.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(25.3211F, -64.4049F, -38.5F, 9.0F, 2.0F, 2.0F, -0.5F, false);
        Left.setTextureOffset(0, 352).addBox(25.8211F, -72.9049F, -38.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        Left.setTextureOffset(0, 352).addBox(24.8211F, -72.9049F, -38.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);

        light_fright = new EntityGlowRenderer(this);
        light_fright.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(light_fright, 0.0F, 0.0F, -0.0873F);
        light_fright.setTextureOffset(0, 350).addBox(-34.0F, -45.0F, -145.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_fright.setTextureOffset(0, 350).addBox(-35.0F, -46.0F, -144.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);

        light_fleft = new EntityGlowRenderer(this);
        light_fleft.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(light_fleft, 0.0F, 0.0F, 0.0873F);
        light_fleft.setTextureOffset(0, 350).addBox(30.0F, -45.0F, -145.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_fleft.setTextureOffset(0, 350).addBox(29.0F, -46.0F, -144.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);

        light_frightlantern = new EntityGlowRenderer(this);
        light_frightlantern.setRotationPoint(-3.0F, -1.0F, 106.0F);
        setRotationAngle(light_frightlantern, 0.0F, 0.0F, -0.0873F);
        light_frightlantern.setTextureOffset(0, 350).addBox(-34.0F, -45.0F, -145.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_frightlantern.setTextureOffset(0, 350).addBox(-35.0F, -46.0F, -144.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);

        light_fleftlantern = new EntityGlowRenderer(this);
        light_fleftlantern.setRotationPoint(0.0F, -1.0F, 106.0F);
        setRotationAngle(light_fleftlantern, 0.0F, 0.0F, 0.0873F);
        light_fleftlantern.setTextureOffset(0, 350).addBox(30.0F, -45.0F, -145.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_fleftlantern.setTextureOffset(0, 350).addBox(29.0F, -46.0F, -144.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);

        light_rleft = new EntityGlowRenderer(this);
        light_rleft.setRotationPoint(0.0F, -1.0F, 106.0F);
        setRotationAngle(light_rleft, 0.0F, 0.0F, 0.0873F);
        light_rleft.setTextureOffset(6, 386).addBox(39.9207F, -12.9134F, 23.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(6, 386).addBox(40.9207F, -12.9134F, 23.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(6, 386).addBox(48.9207F, -12.9134F, 23.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(0, 386).addBox(39.9207F, -12.9134F, 22.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(6, 386).addBox(40.9207F, -3.9134F, 23.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(11, 390).addBox(43.0996F, -10.0085F, 24.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_rleft.setTextureOffset(11, 390).addBox(42.0996F, -11.0085F, 23.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);

        light_rright = new EntityGlowRenderer(this);
        light_rright.setRotationPoint(-10.0F, 35.0F, -14.0F);
        setRotationAngle(light_rright, 0.0F, 0.0F, -0.0873F);
        light_rright.setTextureOffset(0, 393).addBox(-34.0F, -45.0F, 144.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 393).addBox(-35.0F, -46.0F, 143.0F, 6.0F, 6.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 393).addBox(-36.9207F, -47.9134F, 142.0F, 10.0F, 10.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 384).addBox(-27.9207F, -47.9134F, 143.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 384).addBox(-35.9207F, -47.9134F, 143.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 384).addBox(-36.9207F, -47.9134F, 143.0F, 1.0F, 10.0F, 1.0F, 0.0F, false);
        light_rright.setTextureOffset(0, 384).addBox(-36.4207F, -39.4134F, 142.5F, 9.0F, 2.0F, 2.0F, -0.5F, false);

        light_bumperRearLeft = new EntityGlowRenderer(this);
        light_bumperRearLeft.setRotationPoint(-10.0F, 35.0F, -14.0F);
        light_bumperRearLeft.setTextureOffset(4, 384).addBox(49.0F, -33.0F, 146.0F, 8.0F, 5.0F, 2.0F, 0.0F, false);
        light_bumperRearLeft.setTextureOffset(38, 384).addBox(57.0F, -33.0F, 146.0F, 5.0F, 5.0F, 2.0F, 0.0F, false);

        light_bumperRearRight = new EntityGlowRenderer(this);
        light_bumperRearRight.setRotationPoint(-10.0F, 35.0F, -14.0F);
        light_bumperRearRight.setTextureOffset(4, 384).addBox(-37.0F, -33.0F, 146.0F, 8.0F, 5.0F, 2.0F, 0.0F, false);
        light_bumperRearRight.setTextureOffset(38, 384).addBox(-42.0F, -33.0F, 146.0F, 5.0F, 5.0F, 2.0F, 0.0F, false);

        Steering = new ModelRenderer(this);
        Steering.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(Steering, -0.0873F, 0.0F, 0.0F);
        

        steering_assembly_whole = new ModelRenderer(this);
        steering_assembly_whole.setRotationPoint(-30.0F, -65.4412F, -21.0F);
        Steering.addChild(steering_assembly_whole);
        setRotationAngle(steering_assembly_whole, 0.0F, -3.0543F, 0.0F);
        

        steering_stick = new ModelRenderer(this);
        steering_stick.setRotationPoint(0.0F, 51.4412F, 0.0F);
        steering_assembly_whole.addChild(steering_stick);
        steering_stick.setTextureOffset(0, 0).addBox(-2.0F, -55.0F, -2.0F, 4.0F, 57.0F, 4.0F, 0.0F, false);

        steering_wheel = new ModelRenderer(this);
        steering_wheel.setRotationPoint(0.0F, -0.5588F, 0.0F);
        steering_assembly_whole.addChild(steering_wheel);
        setRotationAngle(steering_wheel, 0.0F, -0.0873F, 0.0F);
        steering_wheel.setTextureOffset(0, 0).addBox(-2.0F, -3.0F, 2.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-11.0F, -3.0F, 2.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-12.0F, -3.0F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(8.0F, -3.0F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-11.0F, -3.0F, -10.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(7.0F, -3.0F, -10.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(7.0F, -3.0F, 2.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-2.0F, -3.0F, -10.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-10.0F, -3.0F, 7.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-4.0F, -3.0F, 8.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-4.0F, -3.0F, -12.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(2.0F, -3.0F, 7.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(2.0F, -3.0F, -11.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-10.0F, -3.0F, -11.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(-10.0F, -3.0F, -2.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        steering_wheel.setTextureOffset(0, 0).addBox(2.0F, -3.0F, -2.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);

        RearLicensPlate = new ModelRenderer(this);
        RearLicensPlate.setRotationPoint(0.0F, 8.0F, -16.0F);
        RearLicensPlate.setTextureOffset(0, 0).addBox(15.0F, -3.0F, 148.0F, 1.0F, 20.0F, 2.0F, 0.0F, false);
        RearLicensPlate.setTextureOffset(0, 0).addBox(-16.0F, -3.0F, 148.0F, 1.0F, 20.0F, 2.0F, 0.0F, false);
        RearLicensPlate.setTextureOffset(0, 0).addBox(-15.0F, 16.0F, 148.0F, 30.0F, 1.0F, 2.0F, 0.0F, false);
        RearLicensPlate.setTextureOffset(0, 0).addBox(-15.0F, -3.0F, 148.0F, 30.0F, 1.0F, 2.0F, 0.0F, false);
        RearLicensPlate.setTextureOffset(0, 0).addBox(-15.0F, -2.0F, 148.0F, 30.0F, 18.0F, 1.0F, 0.0F, false);

        headlights_front_indicators_left = new ModelRenderer(this);
        headlights_front_indicators_left.setRotationPoint(0.0F, 24.0F, 0.0F);
        headlights_front_indicators_left.setTextureOffset(38, 386).addBox(-50.0F, -9.75F, -149.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);
        headlights_front_indicators_left.setTextureOffset(41, 363).addBox(-51.0F, -11.0F, -148.0F, 8.0F, 8.0F, 2.0F, 0.0F, false);

        headlights_front_indicators_right = new ModelRenderer(this);
        headlights_front_indicators_right.setRotationPoint(0.0F, 24.0F, 0.0F);
        headlights_front_indicators_right.setTextureOffset(38, 386).addBox(44.0F, -9.75F, -149.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);
        headlights_front_indicators_right.setTextureOffset(41, 363).addBox(43.0F, -11.0F, -148.0F, 8.0F, 8.0F, 2.0F, 0.0F, false);

        bb_main = new ModelRenderer(this);
        bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
        bb_main.setTextureOffset(0, 178).addBox(-24.0F, -57.828F, -141.4F, 48.0F, 4.0F, 108.0F, 0.0F, false);
        
        LIGHTME = new EntityGlowRenderer(this);
        LIGHTME.setRotationPoint(0.0F, 24.0F, 0.0F);
        LIGHTME.setTextureOffset(38, 386).addBox(44.0F, -9.75F, -149.0F, 6, 6, 2, 0.0F, false);
        LIGHTME.setTextureOffset(38, 386).addBox(-50.0F, -9.75F, -149.0F, 6, 6, 2, 0.0F, false);
        addText();
    }

    @Override
    public void setRotationAngles(BessieEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    
    public void render(BessieEntity entity, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
        float motion = MathHelper.sqrt(Entity.horizontalMag(entity.getMotion()));
        boolean isMoving = motion >= 0.01D;
        double xMotion = entity.getMotion().x;
        matrixStack.push();
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
        matrixStack.translate(0, -0.55, 0);
        matrixStack.scale(0.2f, 0.2f, 0.2f);
        
        //Wheel Moving Rotation
        if (isMoving && entity.getHealth() > 5) {
            Wheel_FrontLeft.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entity.ticksExisted * 100 * motion) : -Math.toRadians(entity.ticksExisted * 100 * motion));
            Wheel_FrontRight.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entity.ticksExisted * 100 * motion) : -Math.toRadians(entity.ticksExisted * 100 * motion));
            back.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entity.ticksExisted * 100 * motion) : -Math.toRadians(entity.ticksExisted * 100 * motion));
        } else {
            Wheel_FrontLeft.rotateAngleX = 0;
            Wheel_FrontRight.rotateAngleX = 0;
            back.rotateAngleX = 0;
        }

        // Turn Steering wheel and wheel direction
        if (entity.isBeingRidden() && isMoving) {
            float wheelRot = MathHelper.clamp(entity.getRotationYawHead(), -22, 22);
            Wheel_FrontLeft.rotateAngleY = (float) Math.toRadians(wheelRot);
            Wheel_FrontRight.rotateAngleY = (float) Math.toRadians(wheelRot);
            steering_wheel.rotateAngleY = (float) Math.toRadians(entity.getRotationYawHead());
        } else {
            steering_wheel.rotateAngleY = (float) Math.toRadians(0);
            Wheel_FrontLeft.rotateAngleY = (float) Math.toRadians(0);
            Wheel_FrontRight.rotateAngleY = (float) Math.toRadians(0);
        }

        //Damaged Bessie rotations
        if (entity.getHealth() <= 5) {
            Wheel_FrontLeft.rotateAngleZ = (float) Math.toRadians(45);
            Wheel_FrontRight.rotateAngleZ = (float) Math.toRadians(-45);
            Wheel_BackLeft.rotateAngleZ = (float) Math.toRadians(45);
            Wheel_BackRight.rotateAngleZ = (float) Math.toRadians(-45);
        } else {
            Wheel_FrontLeft.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_FrontRight.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_BackLeft.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_BackRight.rotateAngleZ = (float) Math.toRadians(0);
        }
        
        bb_main.render(matrixStack, buffer, packedOverlay, packedOverlay);
        Floor.render(matrixStack, buffer, packedOverlay, packedOverlay);
        FrontLicensePlate.render(matrixStack, buffer, packedOverlay, packedOverlay);
        RightWall.render(matrixStack, buffer, packedOverlay, packedOverlay);
        LeftWall.render(matrixStack, buffer, packedOverlay, packedOverlay);
        CrossBeams.render(matrixStack, buffer, packedOverlay, packedOverlay);
        Axles.render(matrixStack, buffer, packedOverlay, packedOverlay);
        Spare_Wheel.render(matrixStack, buffer, packedOverlay, packedOverlay);
        Headlights.render(matrixStack, buffer, packedOverlay, packedOverlay);
        Steering.render(matrixStack, buffer, packedOverlay, packedOverlay);
        RearLicensPlate.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_fright.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_fleft.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_frightlantern.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_fleftlantern.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_rleft.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_rright.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_bumperRearLeft.render(matrixStack, buffer, packedOverlay, packedOverlay);
        light_bumperRearRight.render(matrixStack, buffer, packedOverlay, packedOverlay);
    	LIGHTME.render(matrixStack, buffer, packedOverlay, packedOverlay);
        matrixStack.pop();
        
        //Back plate - Originally by Swirtzly, ported by 50ap5ud5
        matrixStack.push();
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
        matrixStack.translate(0, -0.55, 0);
        matrixStack.scale(0.2f, 0.2f, 0.2f);
        RearLicensPlate.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        //Front plate - Originally by Swirtzly, ported by 50ap5ud5
        matrixStack.push();
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
        matrixStack.translate(0, -0.55, 0);
        matrixStack.scale(0.2f, 0.2f, 0.2f);
        FrontLicensePlate.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        renderAllText(matrixStack);
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void addText() {
        this.textToRender.add(new RenderText("Who-3").setFontColor(TextFormatting.WHITE).setPosition(0, 0).centerText().setSmall(false));
    }

    @Override
    public void renderAllText(MatrixStack matrixStack) {
        FontRenderer fr = (Minecraft.getInstance()).fontRenderer;
        float fontZOffset = -0.975F;
        matrixStack.push();
        
        if (fr != null) {
            matrixStack.push();
            matrixStack.translate(0.0F, 0.0F, fontZOffset);
            matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
            matrixStack.translate(0, -0.275, -0.89);
            new RenderText("Who-3").setFontColor(TextFormatting.WHITE).setPosition(0, 0).centerText().setSmall(true).renderText(matrixStack);
            matrixStack.pop();
            
            //Back plate
            matrixStack.push();
            matrixStack.translate(0.0F, 0.0F, fontZOffset);
            matrixStack.rotate(Vector3f.XN.rotationDegrees(180));
            matrixStack.translate(0, -0.425, -2.65);
            new RenderText("Who-3").setFontColor(TextFormatting.WHITE).setPosition(0, 0).centerText().setSmall(true).renderText(matrixStack);
            matrixStack.pop();
        }
        matrixStack.pop();
        
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
            float red, float green, float blue, float alpha) {
        
    }
}