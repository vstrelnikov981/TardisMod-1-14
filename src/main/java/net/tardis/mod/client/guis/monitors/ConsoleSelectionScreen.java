package net.tardis.mod.client.guis.monitors;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.Console;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.TardisRegistries;

import java.util.List;

public class ConsoleSelectionScreen extends MonitorScreen {

    private List<Console> consoles = Lists.newArrayList();
    private int index = 0;

    public ConsoleSelectionScreen(IMonitorGui gui, String name) {
        super(gui, name);
    }

    @Override
    protected void init() {
        super.init();

        this.consoles.clear();
        this.consoles.addAll(ConsoleRegistry.CONSOLE_REGISTRY.get().getValues());

        this.addSubmenu(Constants.Translations.GUI_PREV, (button) -> modIndex(-1));
        this.addSubmenu(Constants.Translations.GUI_SELECT, (button) -> {
            Network.sendToServer(new ConsoleChangeMessage(this.getSelectedConsole().getRegistryName()));
            this.minecraft.displayGuiScreen(null);
        });
        this.addSubmenu(Constants.Translations.GUI_NEXT, (button) -> modIndex(1));

        TardisHelper.getConsoleInWorld(this.getMinecraft().world).ifPresent(tile -> {
            int temp = 0;
            for (Console console : this.consoles) {
                if (console.getState().getBlock() == this.getMinecraft().world.getBlockState(tile.getPos()).getBlock()) {
                    index = temp;
                    break;
                }
                ++temp;
            }
        });
    }

    private Console getSelectedConsole() {
        if (this.index >= this.consoles.size() || this.index < 0)
            return ConsoleRegistry.STEAM.get();
        return this.consoles.get(this.index);
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);

        this.getMinecraft().getTextureManager().bindTexture(this.getSelectedConsole().getPreviewTexture());
        blit(matrixStack, ((this.parent.getMaxX() - this.parent.getMinX()) / 2) + this.parent.getMinX() - 50,
                this.parent.getMaxY(), 0, 0, 100, 100, 100, 100);
    }

    public void modIndex(int mod) {
        if (index + mod > consoles.size() - 1)
            index = 0;
        else if (index + mod < 0)
            index = this.consoles.size() - 1;
        else index += mod;
    }

}
