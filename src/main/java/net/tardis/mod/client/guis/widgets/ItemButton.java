package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;

public class ItemButton extends Button {

    private ItemStack stack;

    public ItemButton(int x, int y, ItemStack stack, ITextComponent title, IPressable press) {
        super(x, y, 16, 16, title, press);
        this.stack = stack;
    }

    @Override
    public void renderWidget(MatrixStack matrixStack, int x, int y, float partialTicks) {
        if (this.active && this.visible) {
            GlStateManager.pushMatrix();
            RenderHelper.setupGuiFlatDiffuseLighting();
            Minecraft.getInstance().getItemRenderer().renderItemAndEffectIntoGUI(this.stack, this.x, this.y);
            GlStateManager.popMatrix();
        }
    }


    @Override
    public void onClick(double p_onClick_1_, double p_onClick_3_) {
        super.onClick(p_onClick_1_, p_onClick_3_);
    }

    public ItemStack getItemStack() {
        return this.stack;
    }

}
