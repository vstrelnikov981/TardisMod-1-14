package net.tardis.mod.client.guis.widgets;

import net.minecraft.client.gui.widget.AbstractSlider;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.LightUpdateMessage;

public class IntSliderWidget extends AbstractSlider {


    public IntSliderWidget(int x, int y, int width, int height, ITextComponent message, double defaultValue) {
        super(x, y, width, height, message, defaultValue);
    }

//	@Override
//	protected void applyValue() {
//
//	}
//
//	@Override
//	protected void updateMessage() {
//		Network.sendToServer(new LightUpdateMessage(this.sliderValue));
//	}

    @Override
    protected void func_230979_b_() {

    }

    @Override
    protected void func_230972_a_() {
        Network.sendToServer(new LightUpdateMessage(this.sliderValue));
    }
}
