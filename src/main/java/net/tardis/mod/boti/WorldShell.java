package net.tardis.mod.boti;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.IBlockDisplayReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeManager;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.level.ColorResolver;
import net.minecraft.world.lighting.WorldLightManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.client.renderers.boti.BotiManager;

import javax.annotation.Nullable;
import java.util.*;
import java.util.Map.Entry;


public class WorldShell implements IBlockDisplayReader {

    public static final int CULL_RANGE = 32;
    //Object type to not make servers sad -- on client will be a BotiWorld
    private static Object botiWorld;
    private static Object botiManager;
    private final int maxRange = 32;
    private HashMap<BlockPos, BlockStore> blocks = new HashMap<>();
    private HashMap<BlockPos, TileEntity> tileEntities = new HashMap<>();
    private HashMap<UUID, Entity> entities = Maps.newHashMap();
    private boolean update = true;
    private BlockPos offset = BlockPos.ZERO;
    private RegistryKey<Biome> biome = Biomes.WARPED_FOREST;
    private BiomeManager biomeManager;
    private RegistryKey<DimensionType> type;
    private Direction portalFacing = Direction.NORTH;
    private HashMap<BlockPos, BlockStore>[] sortedBlocks;

    //Cached bits for the Client
    private Biome cachedBiome;


    public WorldShell(BlockPos offset, RegistryKey<DimensionType> type, RegistryKey<Biome> biome) {
        this.offset = offset.toImmutable();
        this.type = type;
        this.biome = biome;
    }
    
    public WorldShell(BlockPos offset, World world) {
        this.offset = offset.toImmutable();
        this.type = world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY)
        		.getOptionalKey(world.getDimensionType()).orElse(DimensionType.OVERWORLD);
        this.biome = world.func_241828_r().getRegistry(Registry.BIOME_KEY).getOptionalKey(world.getBiome(offset)).orElse(Biomes.FOREST);
    }

    public static WorldShell readFromBuffer(PacketBuffer buffer) {
        WorldShell shell = new WorldShell(buffer.readBlockPos(), RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, buffer.readResourceLocation()), RegistryKey.getOrCreateKey(Registry.BIOME_KEY, buffer.readResourceLocation()));
        shell.setPortalDirection(Direction.byHorizontalIndex(buffer.readInt()));
        int size = buffer.readInt();
        for (int i = 0; i < size; ++i) {
            shell.put(buffer.readBlockPos(), new BlockStore(buffer));
        }
        shell.sortBlocks();
        return shell;
    }

    public void writeToBuffer(PacketBuffer buf) {
        buf.writeBlockPos(offset);
        buf.writeResourceLocation(this.type.getLocation());
        buf.writeResourceLocation(this.biome.getLocation());
        buf.writeInt(this.portalFacing.getHorizontalIndex());
        buf.writeInt(this.blocks.size());
        for (Entry<BlockPos, BlockStore> entry : this.getMap().entrySet()) {
            buf.writeBlockPos(entry.getKey());
            entry.getValue().encode(buf);
        }
    }

    /**
     * MUST call {@link #sortBlocks()} After!
     * @param pos
     * @param store
     */
    public void put(BlockPos pos, BlockStore store) {
        this.blocks.put(pos, store);
    }

    public BlockStore get(BlockPos pos) {
        return this.blocks.get(pos);
    }

    public Map<BlockPos, BlockStore> getMap() {
        return this.blocks;
    }

    public void setMap(HashMap<BlockPos, BlockStore> newShell) {
        this.blocks.clear();
        this.blocks.putAll(newShell);
        this.sortBlocks();
    }

    public void setOffset(BlockPos offset) {
        this.offset = offset;
    }

    @Override
    public TileEntity getTileEntity(BlockPos pos) {
        return tileEntities.get(pos);
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        return get(pos) != null ? get(pos).getState() : Blocks.AIR.getDefaultState();
    }

    @Override
    public FluidState getFluidState(BlockPos pos) {
        return get(pos) != null ? get(pos).getState().getFluidState() : Fluids.EMPTY.getDefaultState();
    }

    public void setBiome(RegistryKey<Biome> b) {
        this.biome = b;
    }

    public Biome getBiome(World world){
        if(this.cachedBiome == null){
            this.cachedBiome = world.func_241828_r().getRegistry(Registry.BIOME_KEY).getValueForKey(this.biome);
        }
        return this.cachedBiome;
    }

    @Nullable
    @OnlyIn(Dist.CLIENT)
    public BotiWorld getWorld() {

       if(botiWorld == null)
    	   botiWorld = BotiWorld.copy(Minecraft.getInstance().world);

        return (BotiWorld) botiWorld;
    }

    @OnlyIn(Dist.CLIENT)
    public void setWorld(BotiWorld world) {
        botiWorld = world;
        for (TileEntity te : this.tileEntities.values())
            te.setWorldAndPos(world, te.getPos());
    }

    @OnlyIn(Dist.CLIENT)
    public BotiManager getRenderManager() {
        if (botiManager == null)
            this.setupRenderManager();
        return (BotiManager) botiManager;
    }

    @OnlyIn(Dist.CLIENT)
    public void setupRenderManager() {
        botiManager = new BotiManager();
        BotiManager boti = (BotiManager) botiManager;
        boti.worldRenderer = new WorldRenderer(Minecraft.getInstance(), Minecraft.getInstance().getRenderTypeBuffers());
        boti.world = BotiWorld.copy(Minecraft.getInstance().world);
        boti.worldRenderer.setWorldAndLoadRenderers(boti.world);
        this.setWorld(boti.world);
    }

    public Map<BlockPos, TileEntity> getTiles() {
        return tileEntities;
    }

    public boolean needsUpdate() {
        return this.update;
    }

    public void setNeedsUpdate(boolean update) {
        this.update = update;
    }

    public BlockPos getOffset() {
        return this.offset;
    }

    @Nullable
    public DimensionType getDimensionType() {
        return this.getWorld().func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getOptionalValue(type).get();
    }

    public void setDimensionType(RegistryKey<DimensionType> type) {
        this.type = type;
    }


    @OnlyIn(Dist.CLIENT)
    public void updateEntities(List<EntityStorage> entities) {

        for (EntityStorage storage : entities) {
            if (this.entities.containsKey(storage.id)) {
                Entity e = this.entities.get(storage.id);
                storage.updateEntity(e);
                if (!e.isAlive() || !e.getPosition().withinDistance(this.getOffset(), 32))
                    this.entities.remove(storage.id);
            } else if (this.getWorld() != null) {
                EntityType<?> type = ForgeRegistries.ENTITIES.getValue(storage.type);
                if (type != null) {
                    Entity e = type.create(this.getWorld());
                    e.setUniqueId(storage.id);

                    try {
                        e.read(storage.data);
                    } catch (Exception exception) {
                    }

                    storage.updateEntity(e);
                    this.entities.put(storage.id, e);
                }
            }
        }

    }

    public void addTile(BlockPos pos, TileEntity entity) {
        this.tileEntities.put(pos, entity);
        entity.updateContainingBlockInfo();
    }

    public void sortBlocks(){
        sortedBlocks = new HashMap[BlockRenderType.values().length];
        int i = 0;
        for(BlockRenderType type : BlockRenderType.values()){
            sortedBlocks[i] = new HashMap<>();
            for(Entry<BlockPos, BlockStore> entry : this.blocks.entrySet()){
                if(entry.getValue().getState().getRenderType() == type)
                    sortedBlocks[i].put(entry.getKey(), entry.getValue());
            }
            ++i;
        }
    }

    @OnlyIn(Dist.CLIENT)
    public Collection<Entity> getEntities() {
        return this.entities.values();
    }

    public void tick(boolean remote) {
        if (remote)
            this.tickWorld();
    }

    @OnlyIn(Dist.CLIENT)
    private void tickWorld() {
        try {
            if (this.getWorld() != null) {
                this.getWorld().tickEntities();
            }
        }
        catch (Exception e) {}
    }

    public void combine(WorldShell shell) {
        if (!shell.getMap().isEmpty() && !shell.getMap().equals(this.getMap())) {
            this.blocks.putAll(shell.getMap());
            this.tileEntities.putAll(shell.tileEntities);
            this.setNeedsUpdate(true);
            this.sortBlocks();
        }
    }

	@Override
	public float func_230487_a_(Direction p_230487_1_, boolean p_230487_2_) {
		return 0;
	}

	@Override
	public WorldLightManager getLightManager() {
		return this.getWorld().getLightManager();
	}

	@Override
	public int getBlockColor(BlockPos blockPosIn, ColorResolver colorResolverIn) {
		return 0;
	}

	public void setPortalDirection(Direction dir) {
		this.portalFacing = dir;
	}
	
	public Direction getPortalDirection() {
		return this.portalFacing;
	}

	public HashMap<BlockPos, BlockStore>[] getSortedBlocks(){
        return this.sortedBlocks;
    }

    //Done the way it is to prevent CMEs
    public void cullOutOfRangeBlocks() {
        List<BlockPos> positionsToRemove = Lists.newArrayList();

        //Gather positions out of range
        for(BlockPos pos : this.blocks.keySet()){
            if(!pos.withinDistance(this.offset, CULL_RANGE))
                positionsToRemove.add(pos);
        }

        //Remove Gathered Positions
        for(BlockPos pos : positionsToRemove){
            this.getMap().remove(pos);
        }

        //Re-sort into layers
        this.sortBlocks();
    }

    public void buildLighting(){
        for(Entry<BlockPos, BlockStore> entry : getMap().entrySet()){
            //If this produces light
            if(entry.getValue().getLight() > 0){

            }
        }
    }
}
