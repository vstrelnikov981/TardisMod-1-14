package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.network.Network;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public class MonitorDynamicTile extends TileEntity implements IMonitor{

	public static final int MAX_WIDTH = 32;
	
	private MonitorView view = MonitorView.PANORAMIC;
	
	public MonitorDynamicTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public MonitorDynamicTile() {
		super(TTiles.DYNAMIC_MONITOR.get());
	}

	public boolean isCorener() {
		Direction dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).rotateYCCW();
		if(this.isStateVaildMonitor(this.getWorld().getBlockState(this.getPos().offset(dir))))
			return false;
		if(this.isStateVaildMonitor(this.getWorld().getBlockState(this.getPos().up())))
			return false;
		return true;
	}
	
	public boolean isStateVaildMonitor(BlockState state) {
		return state.getBlock() == TBlocks.dynamic_monitor.get() &&
				this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING) &&
				state.get(BlockStateProperties.HORIZONTAL_FACING).getAxis() == this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).getAxis();
	}

	public int getWidth() {
		Direction dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).rotateY();
		
		int width = 1;
		
		for(int x = 1; x < MAX_WIDTH; ++x) {
			if(this.isStateVaildMonitor(world.getBlockState(this.getPos().offset(dir, x))))
				++width;
			else break;
		}
		
		return width;
	}
	
	public int getHeight() {
		
		int height = this.getHeightAtPos(this.getPos());
		int width = this.getWidth();
		
		Direction dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).rotateY();
		
		for(int x = 1; x < width; ++x) {
			int newHeight = this.getHeightAtPos(this.getPos().offset(dir, x));
			if(newHeight < height)
				height = newHeight;
		}
		
		return height;
	}
	
	public int getHeightAtPos(BlockPos pos) {
		int height = 1;
		for(int i = 1; i < MAX_WIDTH; ++i) {
			if(this.isStateVaildMonitor(world.getBlockState(pos.offset(Direction.DOWN, i))))
				++height;
			else break;
		}
		return height;
	}
	
	public BlockPos getMaster() {
		
		int y = 0;
		
		for(int i = 1; i < MAX_WIDTH; ++i) {
			if(this.isStateVaildMonitor(world.getBlockState(getPos().offset(Direction.UP, i))))
				++y;
			else break;
		}
		
		Direction dir = this.getFacing().rotateYCCW();
		int x = 0;
		for(int i = 1; i < MAX_WIDTH; ++i) {
			if(this.isStateVaildMonitor(this.world.getBlockState(this.getPos().offset(dir, i))))
				++x;
			else break;
		}
		
		return this.getPos().offset(dir, x).up(y);
		
	}
	
	public Direction getFacing() {
		return this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		
		if(!this.isCorener())
			return super.getRenderBoundingBox();
		
		return new AxisAlignedBB(this.getPos()).grow(MAX_WIDTH);
	}

	@Override
	public MonitorView getView() {
		return this.view;
	}

	@Override
	public void setView(MonitorView view) {
		
		IMonitor mon = this.getMasterMonitor();
		
		if(mon != this) {
			mon.setView(view);
		}
		
		this.view = view;
		this.updateClient();
		this.markDirty();
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		this.view = MonitorView.values()[compound.getInt("view")];
		super.read(state, compound);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("view", this.view.ordinal());
		return super.write(compound);
	}
	
	public void updateClient() {
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 3);
	}
	
	public IMonitor getMasterMonitor() {
		if(this.isCorener())
			return this;
		
		BlockPos master = this.getMaster();
		
		if(world.getTileEntity(master) instanceof IMonitor)
			return (IMonitor)world.getTileEntity(master);
		
		return this;
		
	}
}
