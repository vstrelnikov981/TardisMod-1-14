package net.tardis.mod.tileentities.console.misc;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;

public class ArtronUse {
	
	private IArtronType type;
	private float usePerTick = 0F;
	private int timeToDrain = 0;
	
	protected ArtronUse() {}
	
	public ArtronUse(IArtronType type) {
		this.type = type;
		this.usePerTick = type.getUse();
	}
	
	public void setArtronUsePerTick(float usePerTick) {
		this.usePerTick = usePerTick;
	}
	
	public float getArtronUsePerTick() {
		return this.usePerTick;
	}
	
	public boolean isActive() {
		return this.timeToDrain > -1;
	}
	
	public void setTimeToDrain(int time) {
		this.timeToDrain = time;
	}
	
	public void tick(ConsoleTile tile) {
		if(this.timeToDrain >= 0) {
			
			if(this.timeToDrain > 0) {
				tile.setArtron(tile.getArtron() - this.getArtronUsePerTick());
			}
			
			--this.timeToDrain;
		}
	}
	
	public IArtronType getType() {
		return this.type;
	}
	
	public static interface IArtronType{
		float getUse();
		TranslationTextComponent getTranslation();
	}

	public static enum ArtronType implements IArtronType{
		FLIGHT(0F, "flight"),
		FORCEFIELD(1.0F, "forcefield"),
		CONVERTER(1.0F, "converter"),
		ANTIGRAVS(1.0F, "antigravs"),
		INTERIOR_CHANGE(1.0F, "interior_change");

		float use;
		TranslationTextComponent trans;
		
		ArtronType(float use, String name){
			this.use = use;
			trans = new TranslationTextComponent("artronuse.tardis." + name);
		}
		
		@Override
		public float getUse() {
			return this.use;
		}

		@Override
		public TranslationTextComponent getTranslation() {
			return this.trans;
		}
		
	}
}
