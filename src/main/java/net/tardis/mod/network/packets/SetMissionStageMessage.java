package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.missions.MiniMission;

public class SetMissionStageMessage {

	private BlockPos pos;
	private int stage;
	
	public SetMissionStageMessage(BlockPos pos, int stage) {
		this.pos = pos;
		this.stage = stage;
	}
	
	public static void encode(SetMissionStageMessage mes, PacketBuffer buffer) {
		buffer.writeBlockPos(mes.pos);
		buffer.writeInt(mes.stage);
	}
	
	public static SetMissionStageMessage decode(PacketBuffer buf) {
		return new SetMissionStageMessage(buf.readBlockPos(), buf.readInt());
	}
	
	public static void handle(SetMissionStageMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> {
			cont.get().getSender().getServerWorld().getCapability(Capabilities.MISSION).ifPresent(mission -> {
				MiniMission mis = mission.getMissionForPos(mes.pos);
				if(mis != null) {
					mis.setStage(mes.stage);
					
				}
			});
		});
		
		cont.get().setPacketHandled(true);
	}
}
