package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;

public class FlightSubsystem extends Subsystem {

    int flightSecs = 0;
	
	public FlightSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return super.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
	}


	@Override
    public void onTakeoff() {
        this.flightSecs = 0;
    }

    @Override
    public void onLand() {
        this.flightSecs = 0;
    }

    @Override
    public void onFlightSecond() {
        if (flightSecs % 3 == 0)
            this.damage(null, 1);
        ++this.flightSecs;
	}

}
