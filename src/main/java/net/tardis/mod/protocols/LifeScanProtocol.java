package net.tardis.mod.protocols;

import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.IntWrapper;
import net.tardis.mod.tileentities.ConsoleTile;

public class LifeScanProtocol extends Protocol {

	private static AxisAlignedBB BOX = new AxisAlignedBB(-20, -20, -20, 20, 20, 20);
	@Override
	public void call(World world, PlayerEntity player, ConsoleTile console) {
		if(!world.isRemote) {
			IntWrapper wrapPlayer = new IntWrapper();
			IntWrapper wrapMob = new IntWrapper();
			((ServerWorld)world).getEntities().forEach((entity) -> {
				if(entity instanceof ServerPlayerEntity) {
					wrapPlayer.addInt();
				}
				if (entity instanceof MobEntity) {
					wrapMob.addInt();
				}
			});
			for(PlayerEntity entity : world.getEntitiesWithinAABB(PlayerEntity.class, BOX.offset(console.getPos()))) {
				entity.sendStatusMessage(new TranslationTextComponent("message.tardis.scanner", wrapPlayer.getInt(), wrapMob.getInt()), true);
			}
		}
		else ClientHelper.openGUI(Constants.Gui.NONE, new GuiContext());
	}
	
	@Override
	public String getSubmenu() {
		return "security";
	}

}
