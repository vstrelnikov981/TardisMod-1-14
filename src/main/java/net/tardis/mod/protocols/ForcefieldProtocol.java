package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.ForcefieldData;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ForcefieldProtocol extends Protocol {
	
	public static final TranslationTextComponent TRANS_BROKEN = new TranslationTextComponent("protocol.tardis.forcefield_broken");
	public static final TranslationTextComponent TRANS_REPAIR = new TranslationTextComponent("protocol.tardis.forcefield_repair");
	public static final TranslationTextComponent TRANS_ON = new TranslationTextComponent("protocol.tardis.forcefield_on");
	public static final TranslationTextComponent TRANS_OFF = new TranslationTextComponent("protocol.tardis.forcefield_off");
	public static final TranslationTextComponent TRANS_TURNED_ON = new TranslationTextComponent("protocol.tardis.forcefield_turned_on");
	public static final TranslationTextComponent TRANS_TURNED_OFF = new TranslationTextComponent("protocol.tardis.forcefield_turned_off");
	
	@Override
	public void call(World world, PlayerEntity playerIn, ConsoleTile console) {
		if (!world.isRemote) {
		    console.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {
			    shield.setActivated(!shield.isActivated());
			    Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FORCEFIELD, new ForcefieldData(shield.isActivated())), playerIn.world.getDimensionKey(), playerIn.getPosition(), 64);
			    for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(console.getPos()).grow(16))) {
				    if (shield.isActivated() && !shield.canBeUsed()) {//If we're trying to activate this but its not installed/unable to be used, notify nearby players
				    	player.sendStatusMessage(TRANS_BROKEN, true);
				    }
				    else {
				    	player.sendStatusMessage(shield.isActivated() ? TRANS_TURNED_ON : TRANS_TURNED_OFF, true);
				    }
			    }
		    });
		}
		else ClientHelper.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public TranslationTextComponent getDisplayName(ConsoleTile tile) {
		
		ObjectWrapper<TranslationTextComponent> wrapper = ObjectWrapper.create(TRANS_ON);
		
		if (!tile.getSubsystem(ShieldGeneratorSubsystem.class).isPresent()) {
			wrapper.setValue(TRANS_REPAIR);
		}
		
		tile.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(sys -> {
			if(sys.isActivated()) {
				wrapper.setValue(TRANS_OFF);
			}
		});
		
		return wrapper.getValue();
	}

	@Override
	public String getSubmenu() {
		return "security";
	}

}
