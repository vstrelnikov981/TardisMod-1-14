package net.tardis.mod.entity.humanoid;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.humanoids.DoCrewWorkGoal;
import net.tardis.mod.entity.ai.humanoids.SitInChairGoal;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.missions.misc.Dialog;

public class CrewmateEntity extends AbstractHumanoidEntity{


    public static final DataParameter<Integer> VARIANT = EntityDataManager.createKey(CrewmateEntity.class, DataSerializers.VARINT);
	
	public static final ResourceLocation[] TEXTURES = {
			Helper.createRL("textures/entity/crew/1.png"),
			Helper.createRL("textures/entity/crew/2.png"),
			Helper.createRL("textures/entity/crew/3.png"),
			Helper.createRL("textures/entity/crew/4.png"),
			Helper.createRL("textures/entity/crew/5.png"),
			Helper.createRL("textures/entity/crew/6.png"),
			Helper.createRL("textures/entity/crew/7.png")
	};

	
	public CrewmateEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CrewmateEntity(World worldIn) {
		super(TEntities.CREWMATE.get(), worldIn);
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		return null;
	}

	@Override
	public ResourceLocation getSkin() {
		return TEXTURES[this.getDataManager().get(VARIANT)];
	}

	/**
	 * processInteract equivalent
	 * <br> In 1.16.2+ this is essentially a fallback method that is being called if the vanilla logic is not being met on right click
	 */
	@Override
	protected ActionResultType getEntityInteractionResult(PlayerEntity player, Hand hand) {
		return super.getEntityInteractionResult(player, hand);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		//Tasks
		this.goalSelector.addGoal(3, new SitInChairGoal(this, 0.2334, 16));
		this.goalSelector.addGoal(2, new DoCrewWorkGoal(this, 0.2334, 16));
	}
	
	@Override
	protected void registerData() {
		super.registerData();
		this.getDataManager().register(VARIANT, 0);
	}


	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		setupDefaultEquipment();
		if(!worldIn.isRemote())
			this.getDataManager().set(VARIANT, this.rand.nextInt(TEXTURES.length));
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	public void setupDefaultEquipment() {
		if(!world.isRemote()) {
			this.setItemStackToSlot(EquipmentSlotType.FEET, new ItemStack(TItems.SPACE_BOOTS.get()));
		}
	}
	
	@Override
	protected void dropInventory() {
		for (ItemStack stack : this.getArmorInventoryList()) {
			this.entityDropItem(stack, 0.0F);
		}
	}
	
	public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 10D);
    }

    public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		compound.putInt("texture", this.getDataManager().get(VARIANT));
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		this.getDataManager().set(VARIANT, compound.getInt("texture"));
	}
}
