package net.tardis.mod.events;

import net.minecraftforge.eventbus.api.Cancelable;
import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.tileentities.ConsoleTile;

@Cancelable
public class TardisEvent extends Event{

	ConsoleTile tile;
	
	public TardisEvent(ConsoleTile tile) {
		this.tile = tile;
	}
	
	public ConsoleTile getConsole() {
		return tile;
	}
	/** This event allows modders to modify Tardis behaviour after the Tardis has "taken off". 
	 * <p> It is fired after the Tardis exterior blocks have been removed from the start position*/
	public static class Takeoff extends TardisEvent{

		public Takeoff(ConsoleTile tile) {
			super(tile);
		}
		
	}
	/** This event allows modders to modify Tardis behaviour after it has "landed"
	 * <p> It is fired after the Tardis has fully landed. 
	 * <p> At this point the exterior blocks have been placed, but the destination chunk has not been force loaded*/
	public static class Land extends TardisEvent{

		public Land(ConsoleTile tile) {
			super(tile);
		}
		
	}
}
